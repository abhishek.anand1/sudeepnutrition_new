<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgotpassword extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			
            // Your own constructor code
       }
	public function index()
	{
		
		$result['page']="forgotpassword";

	

		$this->load->view( 'forgotpassword');
	}
	public function update()
	{
			
			$uname=$this->input->post('uname');
			$oldpassword=$this->input->post('oldpassword');
			$condition = array(
                   'admin_username'  => $uname,
                   'admin_password'=>md5($oldpassword)
               );
			   
		$users=$this->commonmodel->select($condition,'tbl_admin');
		if(count($users)>0)
		{
			$password=$this->input->post('password');
			
		
				$newdata = array(
                   'admin_password'  => md5($password),
               );
			$condition = array(
                   'admin_username'  => $uname
               );
			  
			$this->commonmodel->update_entry('tbl_admin',$condition,$newdata);
			$this->session->set_flashdata('message', 'Password updated successfully.Please login with new password.');
			redirect(base_url().'admin/login');
		}
		else
		{
			$this->session->set_flashdata('message', 'Invalid Username Or Password');
			redirect(base_url().'admin/forgotpassword');
		}
			
			
		}
		

}
