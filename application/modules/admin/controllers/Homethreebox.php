<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homethreebox extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['threebox']=$this->commonmodel->select(NULL,'tbl_homethreebox');
		$result['page']="listthreebox";
		$result['mainpage']="home";
		$this->template->load('template', 'listthreebox',$result);
		
	}
	
	public function insert()
	{
		if($this->input->post('threeboxid')!=NULL)
		{
			$id=$this->input->post('threeboxid');
			$title1=$this->input->post('title1');
			$title2=$this->input->post('title2');
			$title3=$this->input->post('title3');
			$link1=$this->input->post('link1');
			$link2=$this->input->post('link2');
			$link3=$this->input->post('link3');
			$description1=$this->input->post('content1');
			$description2=$this->input->post('content2');
			$description3=$this->input->post('content3');
			
			

				$newdata = array(
					    'homethreebox_title1'  => $title1,
					    'homethreebox_title2'  => $title2,
						 'homethreebox_title3'  => $title3,
						 'homethreebox_link1'  => $link1,
					    'homethreebox_link2'  => $link2,
						 'homethreebox_link3'  => $link3,
						 'homethreebox_content1'  => $description1,
						  'homethreebox_content2'  => $description2,
						   'homethreebox_content3'  => $description3
				   );
				   
			$file_name1 = $_FILES['image1']['name'];
			if($file_name1!="")
			{
				$file_name1=uniqid()."_".$file_name1;
				$file_tmp =$_FILES['image1']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				$filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."gallery/".$file_name1;
				move_uploaded_file($file_tmp,$filePath);
				$newdata['homethreebox_image1']=$file_name1;
				
			}
			$file_name2 = $_FILES['image2']['name'];
			if($file_name2!="")
			{
				$file_name2=uniqid()."_".$file_name2;
				$file_tmp =$_FILES['image2']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				$filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."gallery/".$file_name2;
				move_uploaded_file($file_tmp,$filePath);
				$newdata['homethreebox_image2']=$file_name2;
				
				
			}
			$file_name3 = $_FILES['image3']['name'];
			if($file_name3!="")
			{
				$file_name3=uniqid()."_".$file_name3;
				$file_tmp =$_FILES['image3']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				$filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."gallery/".$file_name3;
				move_uploaded_file($file_tmp,$filePath);
				$newdata['homethreebox_image3']=$file_name3;
				
				
			}
			
			$condition = array(
                   'homethreebox_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_homethreebox',$condition,$newdata);
		}
		
	
		redirect(base_url().'admin/homethreebox');
		
	}
	
	
	public function edit()
	{
		$result['page']="listthreebox";
		$result['mainpage']="home";
		$id=$this->uri->segment('4');
		$condition = array(
                   'homethreebox_id'  => $id
               );
		$result['threebox']=$this->commonmodel->select($condition,'tbl_homethreebox');
		$this->template->load('template', 'addthreebox',$result);
	}
	
}
