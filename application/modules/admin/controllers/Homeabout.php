<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeabout extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['homeabout']=$this->commonmodel->select(NULL,'tbl_homeabout');
		$result['page']="listhomeabout";
		$result['mainpage']="home";
		$this->template->load('template', 'listhomeabout',$result);
		
	}
	
	public function delete($id)
	{
		$condition = array(
                   'homeabout_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_homeabout',$condition);
			   redirect(base_url().'admin/homeabout');
	}
	public function insert()
	{
		if($this->input->post('homeaboutid')!=NULL)
		{
			$id=$this->input->post('homeaboutid');
			$description=$this->input->post('description');
			$flashline=$this->input->post('flashline');
				$newdata = array(
						 'homeabout_content'  => $description,
						  'homeabout_oneline'  => $flashline
				   );

			
			$condition = array(
                   'homeabout_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_homeabout',$condition,$newdata);
		}
		
	
		redirect(base_url().'admin/homeabout');
		
	}
	
	
	public function edit()
	{
		$result['page']="homeabout";
		$result['mainpage']="home";
		$id=$this->uri->segment('4');
		$condition = array(
                   'homeabout_id'  => $id
               );
		$result['homeabout']=$this->commonmodel->select($condition,'tbl_homeabout');
		$this->template->load('template', 'addhomeabout',$result);
	}
	
}
