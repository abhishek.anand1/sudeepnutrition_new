<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['news']=$this->commonmodel->select(NULL,'tbl_news');
		$result['page']="listnews";
		$this->template->load('template', 'listnews',$result);
		
	}
	public function add()
	{
		
		$result['page']="addnews";
		$this->template->load('template', 'addnews',$result);
		
	}
	public function delete($id)
	{
		$condition = array(
                   'news_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_news',$condition);
			   redirect(base_url().'admin/news');
	}
	public function insert()
	{
		if($this->input->post('newsid')!=NULL)
		{
			$id=$this->input->post('newsid');
			$content=$this->input->post('content');
			$shortcontent=$this->input->post('shortcontent');
			$title=$this->input->post('title');
			$sortorder=$this->input->post('sortorder');
			$featured=$this->input->post('featured');

				$newdata = array(
					   'news_content'  => $content,
					    'news_shortcontent'  => $shortcontent,
						'news_featured'  => $featured,
						'news_sortorder'  => $sortorder,
					   'news_title'  => $title
					   );
			
			$condition = array(
                   'news_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_news',$condition,$newdata);
		}
		else
		{
		
			$content=$this->input->post('content');
			$title=$this->input->post('title');
			$shortcontent=$this->input->post('shortcontent');
			$featured=$this->input->post('featured');
			$sortorder=$this->input->post('sortorder');
			
			$newdata = array(
					   'news_content'  => $content,
					    'news_shortcontent'  => $shortcontent,
						'news_featured'  => $featured,
						'news_sortorder'  => $sortorder,
					   'news_title'  => $title
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_news');
		}
	
		redirect(base_url().'admin/news');
		
	}
	
	
	public function edit()
	{
		$result['page']="news";
		$id=$this->uri->segment('4');
		$condition = array(
                   'news_id'  => $id
               );
		$result['news']=$this->commonmodel->select($condition,'tbl_news');
		$this->template->load('template', 'addnews',$result);
	}
	
}
