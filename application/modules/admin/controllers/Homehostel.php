<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homehostel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
      public function update()
	{
		if($this->input->post())
		{
			$id=1;
			$link=$this->input->post('link');
			$title=$this->input->post('title');
			$file_name = $_FILES['image']['name'];
			
			if($file_name!="")
			{
				$file_tmp =$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				 $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL.'images/'.$file_name;
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
					   'link'  => $link,
					   'title'  => $title,
					   'image'  => $file_name
				   );
			}
			else
			{
				$newdata = array(
					   'link'  => $link,
					   'title'  => $title					 
				   );

			}
			$condition = array(
                   'homehostel_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_homehostel',$condition,$newdata);
		}
		
	
		redirect(base_url().'admin/homehostel/edit');
		
	}
	
	
	public function edit()
	{
		$result['page']="listhomehostel";
		$result['mainpage']="home";

                $result['hostel']=$this->commonmodel->select(NULL,'tbl_homehostel');
		$this->template->load('template', 'edithomehostel',$result);
	}
	
}
