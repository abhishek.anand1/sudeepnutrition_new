<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$join=array("tbl_page"=>"tbl_page.page_id=tbl_menu.menu_page_id");
		$result['menulist']=$this->commonmodel->select_join(NULL,'tbl_menu',$join);
		$result['page']="listmenu";
		$this->template->load('template', 'listmenu',$result);
		
	}
	public function add()
	{
		
		$result['page']="addmenu";
		$condition = array(
                   'page_showinmenu'  => 1,
				   'page_parent'  => 0
               );
		$result['allpages']=$this->commonmodel->select($condition,'tbl_page');
		$result['allmenupriority']=$this->commonmodel->select(NULL,'tbl_menu');
		$this->template->load('template', 'addmenu',$result);
		
	}
	
	public function delete($id)
	{
		$condition = array(
                   'menu_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_menu',$condition);
			   redirect(base_url().'admin/menu');
	}
	public function insert()
	{
		if($this->input->post('menuid')!=NULL)
		{
			$id=$this->input->post('menuid');
			$pageid=$this->input->post('pageid');
			$priority=$this->input->post('priority');
		$newdata = array(
					   'menu_page_id'  => $pageid, 
					   'menu_priority'  => $priority
				   );
			$condition = array(
                   'menu_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_menu',$condition,$newdata);
		   $sortorder=$this->input->post('sortorder');
		   $sortpage=$this->input->post('page_id');
           foreach ($sortorder as $key => $value) {
                            $con=array(
                               'page_id'=> $sortpage[$key]
                            );
                            $data=array(
                                'sort_order'=>$value
                               );
                         	$this->commonmodel->update_entry('tbl_page',$con,$data);
           }
		}
		else
		{
		
			$pageid=$this->input->post('pageid');
			$priority=$this->input->post('priority');
		
			$newdata = array(
					   'menu_page_id'  => $pageid, 
					   'menu_priority'  => $priority
					  
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_menu');
		}
	
		redirect(base_url().'admin/menu');
		
	}
	
	
	public function edit()
	{
		$result['page']="menu";
		$result['allpages']=$this->commonmodel->select(NULL,'tbl_page');
		$result['allmenupriority']=$this->commonmodel->select(NULL,'tbl_menu');
		$id=$this->uri->segment('4');
		$condition = array(
                   'menu_id'  => $id
               );
		$result['menu_id']=$id;
		$result['menulist'] = $this->commonmodel->select($condition,'tbl_menu');
		$result['level1']   = $this->commonmodel->select_dec_asc(array('page_parent'=>$result['menulist'][0]['menu_page_id']),'tbl_page',NULL,'sort_order');
		$this->template->load('template', 'addmenu',$result);
	}
	   public function submenu()
	{
		$result['page']="sub-menu";
		
		$menu_id=$this->uri->segment('4');
		$id=$this->uri->segment('5');
                $con=array(
                               'page_id'=> $id
                            );
                $result['page_data']=$this->commonmodel->select($con,'tbl_page');
       
		$result['menu_id']=$menu_id;
                $result['level2']=$this->commonmodel->select_dec_asc(array('page_parent'=>$id),'tbl_page',NULL,'sort_order');
        
               	$this->template->load('template', 'editsubmenu',$result);
	}
        function savesubmenu() {
             $menu_id=$this->input->post('menu_id');
             $sortorder=$this->input->post('sortorder');
	         $sortpage=$this->input->post('page_id');
             foreach ($sortorder as $key => $value) {
                            $con=array(
                               'page_id'=> $sortpage[$key]
                            );
                            $data=array(
                                'sort_order'=>$value
                                );
                            	$this->commonmodel->update_entry('tbl_page',$con,$data);
                        }
                        redirect('/admin/menu/edit/'.$menu_id);
        }
}
