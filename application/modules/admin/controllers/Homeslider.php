<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeslider extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		//$result['sliders']=$this->commonmodel->select(NULL,'tbl_homeslider');
		$join=array("tbl_page"=>"tbl_page.page_id=tbl_homeslider.homeslider_page");
		$result['sliders']=$this->commonmodel->select_join(NULL,'tbl_homeslider',$join);
		$result['page']="listslider";
		$result['mainpage']="home";
		$this->template->load('template', 'listhomeslider',$result);
		
	}
	public function add()
	{
		
		$result['page']="addslider";
		$result['mainpage']="home";
		$result['allpages']=$this->commonmodel->select(NULL,'tbl_page');
		$this->template->load('template', 'addhomeslider',$result);
		
	}
	public function delete($id)
	{
		$condition = array(
                   'homeslider_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_homeslider',$condition);
			   redirect(base_url().'admin/homeslider');
	}
	public function insert()
	{
		if($this->input->post('sliderid')!=NULL)
		{
			$id=$this->input->post('sliderid');
			$description=$this->input->post('description');
			$title=$this->input->post('title');
			$page=$this->input->post('pageid');
			$file_name = $_FILES['image']['name'];
			
			if($file_name!="")
			{
				$file_tmp =$_FILES['image']['tmp_name'];
				//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
				 $filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."homeslider/".$file_name;
				move_uploaded_file($file_tmp,$filePath);
				
				$newdata = array(
					   'homeslider_description'  => $description,
					   'homeslider_title'  => $title,
					   'homeslider_page'  => $page,
					   'homeslider_image'  => $file_name
				   );
				  
			}
			else
			{
				
				$newdata = array(
					   'homeslider_description'  => $description,
					   'homeslider_title'  => $title,
					   'homeslider_page'  => $page,
				   );

			}
			$condition = array(
                   'homeslider_id'  => $id
               );
			$this->commonmodel->update_entry('tbl_homeslider',$condition,$newdata);
		}
		else
		{
		
			$id=$this->input->post('sliderid');
			$description=$this->input->post('description');
			$title=$this->input->post('title');
			$page=$this->input->post('pageid');
			$file_name = $_FILES['image']['name'];
			$file_tmp =$_FILES['image']['tmp_name'];
			//$filePath=$_SERVER["DOCUMENT_ROOT"]."Resolution_Talent/assets/actorsprofile/".$file_name;
			$filePath=$_SERVER["DOCUMENT_ROOT"].UPLOAD_URL."homeslider/".$file_name;
			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					   'homeslider_description'  => $description,
					   'homeslider_title'  => $title,
					   'homeslider_page'  => $page,
					   'homeslider_image'  => $file_name
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_homeslider');
		}
	
		redirect(base_url().'admin/homeslider');
		
	}
	
	
	public function edit()
	{
		$result['page']="slider";
		$result['mainpage']="home";
		$result['allpages']=$this->commonmodel->select(NULL,'tbl_page');
		$id=$this->uri->segment('4');
		$condition = array(
                   'homeslider_id'  => $id
               );
		$result['slider']=$this->commonmodel->select($condition,'tbl_homeslider');
		$this->template->load('template', 'addhomeslider',$result);
	}
	
}
