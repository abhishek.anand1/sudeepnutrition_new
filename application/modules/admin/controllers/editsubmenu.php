<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>/admin/menu">Menu-List</a></li>
                    <li><a href="<?php echo base_url()?>/admin/menu/edit/<?php echo $list['menu_id']; ?>"><?php echo $list['page_data'][0]['page_name'] ?></a></li>
                    <li><a href="#">Edit</a></li>

                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/menu/savesubmenu" enctype="multipart/form-data" method="post">
                                <input type="hidden" value="<?php echo $list['menu_id']; ?>" name="menu_id">
                                <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Site Menu</strong> Edit Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                               <?php if(isset($list['level2'])){ ?>
                               <div class="panel-body">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SI No</th>
                                                <th>Page Name</th>
                                                <th>Sort Order</th>
                                              
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
					$i=0;
					foreach($list['level2'] as $val)
					{
					$i++; ?>
                                            <tr>
                                                <td><?php echo $i?></td>
                                                <td><?php echo $val['page_name']?></td>
                                                <td><input type="hidden" name="page_id[]" value="<?php echo $val['page_id']?>" /> <input name="sortorder[]" value="<?php echo $val['sort_order']?>"/></td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>                                    
                                    
                                </div>
                               <?php } ?>
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                    <a href="<?php echo base_url()?>/admin/menu/edit/<?php echo $list['menu_id']; ?>" class="btn btn-primary pull-left">Back </a>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        