<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stafflinks extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['content']=$this->commonmodel->select(NULL,'tbl_stafflinks');
		$result['page']="liststafflinks";
		$this->template->load('template', 'liststafflinks',$result);
		
	}
	
	
	public function insert()
	{
		if($this->input->post('id')!=NULL)
		{
			$id=$this->input->post('id');
			$content=$this->input->post('content');

				$newdata = array(
					   'content'  => $content
					   
				   );
			$condition = array(
                   'id'  => $id
               );
			$this->commonmodel->update_entry('tbl_stafflinks',$condition,$newdata);
		}
		
	
		redirect(base_url().'admin/stafflinks');
		
	}
	
	
	public function edit()
	{
		$result['page']="stafflinks";
		$id=$this->uri->segment('4');
		$condition = array(
                   'id'  => $id
               );
		$result['content']=$this->commonmodel->select($condition,'tbl_stafflinks');
		$this->template->load('template', 'addstafflinks',$result);
	}
	
}
