<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			if($this->session->userdata('username') == FALSE)
			{
				redirect('admin/login');
			}
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['events']=$this->commonmodel->select(NULL,'tbl_events');
		$result['images']=$this->commonmodel->select(NULL,'tbl_eventimages');
		$result['page']="listevents";
		$this->template->load('template', 'listevents',$result);
		
	}
	public function add()
	{
		
		$result['page']="addevents";
		$result['college']=$this->commonmodel->select(NULL,'tbl_college',NULL);
		$dept_condition=array('show_in_event'=>1);
		$result['department']=$this->commonmodel->select($dept_condition,'tbl_department',NULL);
		$this->template->load('template', 'addevents',$result);
		
	}
	public function delete($id)
	{
		$condition = array(
                   'events_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_events',$condition);
			   redirect(base_url().'admin/events');
	}
	public function image_delete($id)
	{
		$condition = array(
                   'image_id'  => $id
               );
			   $this->commonmodel->delete_entry('tbl_eventimages',$condition);
			      echo "success";
			  // redirect(base_url().'admin/events');
	}
	
	public function insert()
	{
		if($this->input->post('eventsid')!=NULL)
		{
			$category=$this->input->post('eventcategory');
			$collegeid=$this->input->post('collegeid');
			$departmentid=$this->input->post('departmentid');
			$id=$this->input->post('eventsid');
			$content=$this->input->post('content');
			$shortcontent=$this->input->post('shortcontent');
			$eventdate=$this->input->post('eventdate');
			$eventdate=date('Y-m-d',strtotime($eventdate));
			$sortorder=$this->input->post('sortorder');
			$title=$this->input->post('title');

				$newdata = array(
				'events_category'  => $category,
				'events_collegeid'  => $collegeid,
				'events_departmentid'  => $departmentid,
					   'events_content'  => $content,
					    'events_date'  => $eventdate,
						'events_sortorder'  => $sortorder,
						  'events_shortcontent'  => $shortcontent,
					   'events_title'  => $title
					   );
			
			$condition = array(
                   'events_id'  => $id
               );



			$geteventid = $this->commonmodel->update_entry('tbl_events',$condition,$newdata);



			  $cpt = count($_FILES['image']['name']);
				if(!$_FILES['image']['name'][0]){
				$cpt=0;
				}
  		$condition = array(
                   'event_id'  => $id
               );
 			//$this->commonmodel->delete_entry('tbl_eventimages',$condition);
   for($i=0; $i<$cpt; $i++){

   
		$file_name = "event_".$id."_".$_FILES['image']['name'][$i];
		$file_tmp =$_FILES['image']['tmp_name'][$i];
		
		// 	  //home/content/17/8657417/html/test/application/modules/admin/controllers/Events.php
	
			$filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/events/".$file_name;

			move_uploaded_file($file_tmp,$filePath);
		
		
 				$newdata = array(
						 'event_id'  => $id,
					   'image_title'  => $file_name
				   );

 			$condition = array(
                   'event_id'  => $id
               );
 		
			 $this->commonmodel->insertagain_entry('tbl_eventimages',$condition,$newdata);
		}

		}
		else
		{
			


			$category=$this->input->post('eventcategory');
			$collegeid=$this->input->post('collegeid');
			$departmentid=$this->input->post('departmentid');
			$content=$this->input->post('content');
			$title=$this->input->post('title');
			$eventdate=$this->input->post('eventdate');
			$shortcontent=$this->input->post('shortcontent');
			$sortorder=$this->input->post('sortorder');
			$eventdate=date('Y-m-d',strtotime($eventdate));
			
			$newdata = array(
			'events_category'  => $category,
				'events_collegeid'  => $collegeid,
				'events_departmentid'  => $departmentid,
					   'events_content'  => $content,
					    'events_date'  => $eventdate,
						'events_sortorder'  => $sortorder,
						  'events_shortcontent'  => $shortcontent,
					   'events_title'  => $title,
				   );
		$geteventid = $this->commonmodel->insert_entry($newdata,'tbl_events');
$cpt=0;
  $cpt = count($_FILES['image']['name']);
  //echo $cpt;
 
   for($i=0; $i<$cpt; $i++){

   
		$file_name ="event_".$geteventid."_". $_FILES['image']['name'][$i];
		$file_tmp =$_FILES['image']['tmp_name'][$i];
		
		// 	  //home/content/17/8657417/html/test/application/modules/admin/controllers/Events.php
	
			$filePath=$_SERVER["DOCUMENT_ROOT"]."/assets/events/".$file_name;

			move_uploaded_file($file_tmp,$filePath);
			$newdata = array(
					   'event_id'  => $geteventid,
					   'image_title'  => $file_name
				   );
			 $this->commonmodel->insert_entry($newdata,'tbl_eventimages');
		}
		//print_r($geteventid);
		}
	
		redirect(base_url().'admin/events');
		
	}
	
	
	public function edit()
	{
		$result['page']="events";
		$id=$this->uri->segment('4');
		$condition = array(
                   'events_id'  => $id
               );
		$result['events']=$this->commonmodel->select($condition,'tbl_events');
		$result['college']=$this->commonmodel->select(NULL,'tbl_college',NULL);

	

		$condition2 = array(
				   'event_id'  => $id
			       );
		$result['images']=$this->commonmodel->select($condition2,'tbl_eventimages',NULL);

		$result['department']=$this->commonmodel->select(NULL,'tbl_department',NULL);

		$this->template->load('template', 'addevents',$result);
	}
	
}
