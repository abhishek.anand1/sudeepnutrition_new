
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Enquiries List</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span>Enquiries List</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"></h3>
                                                                        
                                    
                                </div>
                                <div class="panel-body">
                                                <a href="<?php echo base_url().'admin/enquiries/export_to_excel';?>" class="btn btn-info" >Export to Excel </a>
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                  <th>Category</th>
                                                  <th>From Page</th>
                                                  <th>Name</th>
                                                  <th>Email</th>
                                                  <th>Phone</th>
                                                  <th>Subjetc</th>
                                                  <th>Date</th>
                                                  <th>Message</th>
                          							<th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
										$i=0;
										foreach($list['enquiries'] as $row)
										{
											$i++;
											?>
                                            <tr>
                                                <td><?php echo $i?></td>
                                                <td><?php echo $row["enquiries_category"];?></td>
                                                <td><?php echo $row["enquiries_frompage"];?></td>
                                                <td><?php echo $row["enquiries_name"];?></td>
                                                <td><?php echo $row["enquiries_email"];?></td>
                                                <td><?php echo $row["enquiries_phone"];?></td>
                                                 <td><?php echo $row["enquiries_suject"];?></td>
                                                <td><?php  echo date('d-F-Y', strtotime($row["enquiries_date"]));?></td>
                                                <td><?php echo $row["enquiries_message"];?></td>
                                                <td><a href="enquiries/delete/<?php echo $row['enquiries_id']?> "  class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item');"><i class="glyphicon glyphicon-remove"></i></a></td>
                                                
                                            </tr>
                                            <?php
										}
										?>
                                        </tbody>
                                    </table>                                    
                                    
                                </div>
                
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                            
                            <!-- START DEFAULT TABLE EXPORT -->
                            
                            <!-- END DEFAULT TABLE EXPORT -->

                        </div>
                    </div>

                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->    

        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->        
        
        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
      






