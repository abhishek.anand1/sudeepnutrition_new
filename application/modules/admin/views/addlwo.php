<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">LWO</a></li>
                    <li><a href="#">Edit</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/lwo/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>LWO</strong> Edit Form</h3>
                                    <ul class="panel-controls">
                                        
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body"> 
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Content</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                <textarea class="form-control tinymceeditor" rows="5"  name="content"><?php if(isset($list['content'])){ echo $list['content'][0]['content']; }?></textarea>  
                                                     
                                        <input type="hidden" value="<?php if(isset($list['content'])){ echo $list['content'][0]['id']; }?>" class="form-control" name="id" id="id"/>                                                                 
                                             
                                        </div>
                                    </div>  
                                                                                                     
                                   
                                </div>
                               
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
             
        