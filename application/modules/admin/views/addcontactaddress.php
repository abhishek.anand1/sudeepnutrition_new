<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Contact Address</a></li>
                    <li><a href="#">Edit</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/contactaddress/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Contact Address</strong> Edit Form</h3>
                                    <ul class="panel-controls">
                                        
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body"> 
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Corporate Address</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                <textarea class="form-control tinymceeditor" rows="5"  name="corporateaddress"><?php if(isset($list['contactaddress'])){ echo $list['contactaddress'][0]['contactaddress_address']; }?></textarea>  
                                                     
                                                                                                        
                                             
                                        </div>
                                    </div>  
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Campus Address</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="hidden" value="<?php if(isset($list['contactaddress'])){ echo $list['contactaddress'][0]['contactaddress_id']; }?>" class="form-control" name="contactaddressid" id="contactaddressid"/> 
                                                     
                                                 <textarea class="form-control tinymceeditor" rows="5"  name="campusaddress"><?php if(isset($list['contactaddress'])){ echo $list['contactaddress'][0]['contactaddress_campusaddress']; }?></textarea>                                                              
                                             
                                        </div>
                                    </div>                                                                     
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Phone</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['contactaddress'])){ echo $list['contactaddress'][0]['contactaddress_phone']; }?>" class="form-control" name="phone" id="phone" required="required"/> 
                                                     
                                                                                                             
                                             
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Email</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['contactaddress'])){ echo $list['contactaddress'][0]['contactaddress_email']; }?>" class="form-control" name="email" id="email" required="required"/> 
                                                     
                                                                                                             
                                             
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
             
        