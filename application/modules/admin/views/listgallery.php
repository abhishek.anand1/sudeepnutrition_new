
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>admin/dashboard">Home</a></li>
                    <li class="active">Gallery List</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Gallery List</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"></h3>
                                                                        
                                    
                                </div>
                                <div class="panel-body">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SI No</th>
                                                <th>Title</th>
                                              <?php /*?>  <th>Description</th><?php */?>
                                                <th>Image</th>
                                                 <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
										$i=0;
										foreach($list['gallerys'] as $val)
										{
											$i++;
											?>
                                            <tr>
                                                <td><?php echo $i?></td>
                                                 <td><?php echo $val['gallery_title']?></td>
                                               <?php /*?> <td><?php echo $val['gallery_description']?></td><?php */?>
                                               
                                                <td><img style="width:100px;heght:100px" src="<?php echo base_url()?>assets/gallery/<?php echo $val['gallery_image']?>" alt="<?php echo $val['gallery_id']?>" /></td>
                                                <td><a href="gallery/edit/<?php echo $val['gallery_id']?>" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a></td>
                                                <td><a href="gallery/delete/<?php echo $val['gallery_id']?> "  class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a></td>
                                                
                                            </tr>
                                            <?php
										}
										?>
                                        </tbody>
                                    </table>                                    
                                    
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                            
                            <!-- START DEFAULT TABLE EXPORT -->
                            
                            <!-- END DEFAULT TABLE EXPORT -->

                        </div>
                    </div>

                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->    

        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->        
        
        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
      






