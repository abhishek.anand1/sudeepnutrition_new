<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>T John College</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="robots" content="noarchive">
        <link rel="icon" href="favicon.ico" type="image/x-icoeeen" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url()?>assets/admin_css/theme-default.css"/>
        <?php /*?><link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/upload_js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" /><?php */?>
        
        <!-- EOF CSS INCLUDE -->  
            <input type="hidden" id="base_url" value="<?php echo base_url();?>">                                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?php echo base_url()?>admin/dashboard">T John College</a>
                    </li>
                
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?php echo base_url()?>assets/adminprofile/<?php echo @$list['adminuser'][0]['admin_image'];?>" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?php echo base_url()?>assets/adminprofile/<?php echo @$list['adminuser'][0]['admin_image'];?>" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?php echo @$list['adminuser'][0]['admin_dname'];?></div>
                                
                            </div>
                            <div class="profile-controls">
                                <a href="<?php echo base_url()?>admin/editadminprofile" class="profile-control-left"><span class="fa fa-info"></span></a>
                                
                            </div>
                        </div>                                                                        
                    </li>
                  
                    <li <?php if(@$list['page']=="dashboard"){?> class="active" <?php } ?>>
                        <a href="<?php echo base_url()?>admin/dashboard"><span class="fa fa-dashboard"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>
                                     <li class="xn-openable <?php if(@$list['page']=="page" || @$list['page']=="addpage" || @$list['page']=="listpage" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa  fa-tasks"></span> <span class="xn-text">Pages</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="addpage"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/page/add"><span class="fa fa-plus"></span> Add</a></li>
                                            <li class="<?php if( @$list['page']=="listpage" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/page"><span class="fa fa-list-ul"></span> List</a></li>
                                        </ul>
                                    </li> 
                                    <li class="xn-openable <?php if(@$list['page']=="menu" || @$list['page']=="addmenu" || @$list['page']=="listmenu" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa  fa-tasks"></span> <span class="xn-text">Main Menu</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="addmenu"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/menu/add"><span class="fa fa-plus"></span> Add</a></li>
                                            <li class="<?php if( @$list['page']=="listmenu" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/menu"><span class="fa fa-list-ul"></span> List</a></li>
                                        </ul>
                                    </li> 
                                     <li class="xn-openable <?php if(@$list['mainpage']=="home"){?> active <?php } ?>">
                                     <a href="#"><span class="fa  fa-tasks"></span> <span class="xn-text">Home Page</span></a>
                                        <ul>
                                       <li class="xn-openable <?php if(@$list['page']=="slider" || @$list['page']=="addslider" || @$list['page']=="listslider" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">Home page sliders</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="addslider"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/homeslider/add"><span class="fa fa-plus"></span> Add</a></li>
                                            <li class="<?php if( @$list['page']=="listslider" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/homeslider"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li> 
                                     <li class="xn-openable <?php if(@$list['page']=="listthreebox" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">Three Box</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="listthreebox"  ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/homethreebox"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                     <li class="xn-openable <?php if(@$list['page']=="flash" ||@$list['page']=="listflashmessage"  || @$list['page']=="addflashmessage"  ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">Academic Calendar</span></a>
                                        <ul>
                                         <li class="<?php if( @$list['page']=="addflashmessage"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/flashmessage/add"><span class="fa fa-plus"></span> Add</a></li>
                                            <li class="<?php if( @$list['page']=="listflashmessage" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/flashmessage"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                      <li class="xn-openable <?php if(@$list['page']=="placements" || @$list['page']=="addplacements" || @$list['page']=="listplacements" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">Placements</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="addplacements"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/placements/add"><span class="fa fa-plus"></span> Add</a></li>
                                            <li class="<?php if( @$list['page']=="listplacements" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/placements"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                        <li class="xn-openable <?php if( @$list['page']=="listhomeabout" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">About Section</span></a>
                                        <ul>
                                           
                                            <li class="<?php if( @$list['page']=="listhomeabout" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/homeabout"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li> 
                                    <li class="<?php if( @$list['page']=="listhomehostel" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/homehostel/edit"><span class="fa fa-edit"></span> Hostel</a></li> 
                                     <?php /*?> <li class="xn-openable <?php if( @$list['page']=="listhomenews" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">News and Events</span></a>
                                        <ul>
                                           
                                            <li class="<?php if( @$list['page']=="listhomenews" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/homenews"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li> <?php */?>
                                    </ul>
                                    </li>
                                    <li class="xn-openable <?php if( @$list['page']=="listnews" ||  @$list['page']=="addnews" ||  @$list['page']=="news" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">News</span></a>
                                        <ul>
                                           <li class="<?php if( @$list['page']=="addnews"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/news/add"><span class="fa fa-plus"></span> Add</a></li> 
                                            <li class="<?php if( @$list['page']=="listnews" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/news"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                    <li class="xn-openable <?php if( @$list['page']=="listevents" ||   @$list['page']=="addevents"  ||   @$list['page']=="events"){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-gears"></span> <span class="xn-text">Events</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="addevents"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/events/add"><span class="fa fa-plus"></span> Add</a></li>
                                            <li class="<?php if( @$list['page']=="listevents" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/events"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                     <li class="xn-openable <?php if(@$list['page']=="gallery" || @$list['page']=="addgallerynew" || @$list['page']=="fullgallery" ){?> active <?php } ?>">
                                        <a href="<?php echo base_url()?>admin/gallery/addnew"><span class="fa  fa-image"></span> <span class="xn-text">Gallery</span></a>
                                        <!-- <ul>
                                            <li class="<?php if( @$list['page']=="addgallery"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/gallery/addnew"><span class="fa fa-plus"></span> Add</a></li>
                                            <li class="<?php if( @$list['page']=="listgallery" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/gallery"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul> -->
                                    </li> 
                                     <li class="xn-openable <?php if(@$list['page']=="press" || @$list['page']=="addpress" || @$list['page']=="listpress" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa  fa-image"></span> <span class="xn-text">Press</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="addpress"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/press/add"><span class="fa fa-plus"></span> Add</a></li>
                                            <li class="<?php if( @$list['page']=="listpress" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/press"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li> 
                                    
                                     <li class="xn-openable <?php if(@$list['page']=="media" || @$list['page']=="addmedia" || @$list['page']=="listmedia" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa  fa-image"></span> <span class="xn-text">Media Uploads</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="addmedia"){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/media/add"><span class="fa fa-plus"></span> Add</a></li>
                                            <li class="<?php if( @$list['page']=="listmedia" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/media"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li> 
                                    <li class="xn-openable <?php if(@$list['page']=="contactaddress" || @$list['page']=="listcontactaddress" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">Contact Address</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="listcontactaddress" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/contactaddress"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                           <li class="xn-openable <?php if(@$list['page']=="digitallibrary" || @$list['page']=="lwo" || @$list['page']=="nss" || @$list['page']=="iqac" || @$list['page']=="iso" || @$list['page']=="listdigitallibrary" || @$list['page']=="listlwo" || @$list['page']=="listnss" || @$list['page']=="listiqac" || @$list['page']=="listiso" || @$list['page']=="listfooterlinks" || @$list['page']=="liststafflinks"  ){?> active <?php } ?>">
                                        <a href="#"><span class="fa  fa-tasks"></span> <span class="xn-text">Footer Pages</span></a>
                                        <ul>
                                         <li class="xn-openable <?php if(@$list['page']=="stafflinks" || @$list['page']=="liststafflinks" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">Staff Links</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="liststafflinks" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/stafflinks"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                      <li class="xn-openable <?php if(@$list['page']=="digitallibrary" || @$list['page']=="listdigitallibrary" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">TJIT Digital Library</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="listdigitallibrary" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/digitallibrary"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                      <li class="xn-openable <?php if(@$list['page']=="lwo" || @$list['page']=="listlwo" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">Library Web OPAC</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="listlwo" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/lwo"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                    <li class="xn-openable <?php if(@$list['page']=="nss" || @$list['page']=="listnss" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">NSS Cell</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="listnss" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/nss"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                    <li class="xn-openable <?php if(@$list['page']=="iqac" || @$list['page']=="listiqac" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">IQAC</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="listiqac" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/iqac"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                    <li class="xn-openable <?php if(@$list['page']=="iso" || @$list['page']=="listiso" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">ISO 9001:2008</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="listiso" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/iso"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                        <li class="xn-openable <?php if(@$list['page']=="footerlinks" || @$list['page']=="listfooterlinks" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">Links</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="listfooterlinks" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/footerlinks"><span class="fa fa-list-ul"></span> View</a></li>
                                        </ul>
                                    </li>
                                    </ul>
                                    </li>
                                     <li class="xn-openable <?php if(@$list['page']=="enquiries" || @$list['page']=="listenquiries" ){?> active <?php } ?>">
                                        <a href="#"><span class="fa fa-map-marker"></span> <span class="xn-text">Enquiries</span></a>
                                        <ul>
                                            <li class="<?php if( @$list['page']=="listenquiries" ){?> active <?php } ?>"><a href="<?php echo base_url()?>admin/enquiries"><span class="fa fa-list-ul"></span> View</a></li>
                                            <li class="<?php if( @$list['page']=="listscholarships" ){?> active <?php } ?>"><a href="<?php echo base_url('admin/enquiries/scholarships')?>"><span class="fa fa-list-ul"></span>Scholarship Enqs</a></li>
                                             <li><a href="<?php echo base_url()?>admission/admin/enquiry.php" target="_blank"><span class="fa fa-list-ul"></span> Admission Campaign Enquiries</a></li>
                                        </ul>
                                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                   <!--  <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>  --> 
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- MESSAGES -->
                    <!--<li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-comments"></span></a>
                        <div class="informer informer-danger">4</div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>                                
                                <div class="pull-right">
                                    <span class="label label-danger">4 new</span>
                                </div>
                            </div>
                            <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-online"></div>
                                    <img src="assets/images/users/user2.jpg" class="pull-left" alt="John Doe"/>
                                    <span class="contacts-title">John Doe</span>
                                    <p>Praesent placerat tellus id augue condimentum</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="assets/images/users/user.jpg" class="pull-left" alt="Dmitry Ivaniuk"/>
                                    <span class="contacts-title">Dmitry Ivaniuk</span>
                                    <p>Donec risus sapien, sagittis et magna quis</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="assets/images/users/user3.jpg" class="pull-left" alt="Nadia Ali"/>
                                    <span class="contacts-title">Nadia Ali</span>
                                    <p>Mauris vel eros ut nunc rhoncus cursus sed</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-offline"></div>
                                    <img src="assets/images/users/user6.jpg" class="pull-left" alt="Darth Vader"/>
                                    <span class="contacts-title">Darth Vader</span>
                                    <p>I want my money back!</p>
                                </a>
                            </div>     
                            <div class="panel-footer text-center">
                                <a href="pages-messages.html">Show all messages</a>
                            </div>                            
                        </div>                        
                    </li>-->
                    <!-- END MESSAGES -->
                    <!-- TASKS -->
                    <!--<li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-tasks"></span></a>
                        <div class="informer informer-warning">3</div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-tasks"></span> Tasks</h3>                                
                                <div class="pull-right">
                                    <span class="label label-warning">3 active</span>
                                </div>
                            </div>
                            <div class="panel-body list-group scroll" style="height: 200px;">                                
                                <a class="list-group-item" href="#">
                                    <strong>Phasellus augue arcu, elementum</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 25 Sep 2014 / 50%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Aenean ac cursus</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">80%</div>
                                    </div>
                                    <small class="text-muted">Dmitry Ivaniuk, 24 Sep 2014 / 80%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Lorem ipsum dolor</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">95%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 23 Sep 2014 / 95%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Cras suscipit ac quam at tincidunt.</strong>
                                    <div class="progress progress-small">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 21 Sep 2014 /</small><small class="text-success"> Done</small>
                                </a>                                
                            </div>     
                            <div class="panel-footer text-center">
                                <a href="pages-tasks.html">Show all tasks</a>
                            </div>                            
                        </div>                        
                    </li>-->
                    <!-- END TASKS -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->   
            
        <!-- END PAGE CONTAINER -->

       






