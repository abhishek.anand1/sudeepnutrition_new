<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#"> Placement</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/placements/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong> Placement</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Candidate Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['placements'])){ echo $list['placements'][0]['placements_candidatename']; }?>" class="form-control" required="required" name="candidatename" id="candidatename"/>                                                                      
                                              
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Company Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['placements'])){ echo $list['placements'][0]['placements_company']; }?>" class="form-control" name="companyname" id="companyname" required="required"/>                                                                      
                                              
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Date</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                  <input type="hidden" value="" class="form-control datepicker" data-date-format="dd-mm-yyyy" name="title" id="title"/>                                                                      
                                            <input type="text" class="form-control" value="<?php if(isset($list['placements'])){ echo $list['placements'][0]['placements_date']; }?>" id="dp-4" name="intdate" data-date="07-07-2014" data-date-format="dd-mm-yyyy" data-date-viewmode="months"/>  
                                        </div>
                                    </div>
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Placement Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="hidden" value="<?php if(isset($list['placements'])){ echo $list['placements'][0]['placements_id']; }?>" class="form-control" name="placementsid" id="placementsid"/>                                                                      
                                               <textarea class="form-control" rows="5" name="description"><?php if(isset($list['placements'])){ echo $list['placements'][0]['placements_description']; }?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <?php if(isset($list['placements'])){
									?>
                                <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Candidate Photo</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image"  id="file-simple" />Please upload an image with resolution 260 X 260
                                                        </div>
                                                    </div>
													<?php
								}
								else
								{
									?>
                                     <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Candidate Photo</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                          <input type="file" name="image" required="required" id="file-simple" />Please upload an image with resolution 260 X 260        
                                                        </div>
                                                    </div>
                                    <?php
								}
								?>
													
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        