<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Events</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                             <form onsubmit="return validate();" id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/events/insert" enctype="multipart/form-data" method="post"> 
                           <!--  <form onsubmit="return validate();" id="jvalidate" class="form-horizontal" action="http://www.tjohncollege.com/test/admin/events/insert" enctype="multipart/form-data" method="post"> -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Events</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                       
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                     <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Event Type</label>
                                                        <div class="col-md-6 col-xs-12">                                                                                            
                                                            <select class="form-control select" required="required"  name="eventcategory" id="eventcategory" onchange="category_dispay(this.value)">
<option  >--select--</option>
                                                                        <option value="College" <?php if(@$list['events'][0]['events_category']== 'College'){?> selected="selected" <?php } ?>>College</option>
                                                                        <option value="Department" <?php if(@$list['events'][0]['events_category']== 'Department'){?> selected="selected" <?php } ?>>Department</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                   
                                                     <div class="form-group" id="college">
                                                        <label class="col-md-3 col-xs-12 control-label">College</label>
                                                        <div class="col-md-6 col-xs-12">                                                                                            
                                                            <select class="form-control select" required="required"  name="collegeid" id="collegeid">
																<option  >--select--</option>
                                                                <?php
																
                                                                foreach($list['college'] as $collegeval)
                                                                {
																	
                                                                    ?>
                                                                        <option value="<?php echo $collegeval['college_id'];?>" <?php if(@$list['events'][0]['events_collegeid']== $collegeval['college_id']){?> selected="selected" <?php } ?>><?php echo $collegeval['college_name'];?></option>
                                                                <?php
																
																}
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class="form-group" id="department">
                                                        <label class="col-md-3 col-xs-12 control-label">Department</label>
                                                        <div class="col-md-6 col-xs-12">                                                                                            
                                                            <select class="form-control select" required="required"  name="departmentid" id="departmentid">
																<option  >--select--</option>
                                                                <?php
																
                                                                foreach($list['department'] as $deptval)
                                                                {
																	
                                                                    ?>
                                                                        <option value="<?php echo $deptval['department_id']?>" <?php if(@$list['events'][0]['events_departmentid']== $deptval['department_id']){?> selected="selected" <?php } ?>><?php echo $deptval['department_name'];?></option>
                                                                <?php
																
																}
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                    <div class="form-group">
                                    
                                        <label class="col-md-3 col-xs-12 control-label">Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['events'])){ echo $list['events'][0]['events_title']; }?>" class="form-control" name="title" required="required" id="title"/>                                                                      
                                              
                                        </div>
                                    </div>
                                    
                                      <div class="form-group">
                                    
                                        <label class="col-md-3 col-xs-12 control-label">Event Date</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['events'])){ echo date('d-m-Y',strtotime($list['events'][0]['events_date'])); }?>" class="form-control datepicker"  name="eventdate"  id="eventdate"/>                                                                      
                                              
                                        </div>
                                    </div>
                                     <div class="form-group">
                                    
                                        <label class="col-md-3 col-xs-12 control-label">Sort Order</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="number" value="<?php if(isset($list['events'])){ echo $list['events'][0]['events_sortorder']; }?>" class="form-control" name="sortorder" required="required" id="sortorder"/>                                                                      
                                              
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Short Content</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                                              
                                               <textarea required="required"  class="form-control" id="shortcontent" rows="5" name="shortcontent"><?php if(isset($list['events'])){ echo $list['events'][0]['events_shortcontent']; }?></textarea>
                                                                
                                               
                                               
                                        </div>
                                    </div>
                                <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Content</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="hidden" value="<?php if(isset($list['events'])){ echo $list['events'][0]['events_id']; }?>" class="form-control" name="eventsid" id="eventsid"/>                                                                      
                                               <textarea  class="form-control tinymceeditor" id="content" rows="5" name="content"><?php if(isset($list['events'])){ echo $list['events'][0]['events_content']; }?></textarea>
                                                <label id="content-error" class="test" for="content" style="display:none;color:#E04B4A;">This field is required.</label>                                                   
                                               
                                               
                                        </div>
                                    </div>
                                        <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Images</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                  
                                        <input type="file" name="image[]"  id="file-simple" multiple="multiple" />        
                                                                                           
                                               
                                               
                                        </div>
                                    </div>



                  
                                </div>
<div class="file-preview-thumbnails" id="image_panel">
<?php
if(isset($list['images'])){ 
foreach($list['images'] as $image) {?>
<div class="file-preview-frame" id="preview-1482486589509-0">
   <img src="http://www.tjohncollege.com/assets/events/<?php echo $image['image_title'];?>" class="file-preview-image" title="<?php echo $image['image_title'];?>" alt="<?php echo $image['image_title'];?>" style="width:auto;height:160px;">
   <button type="button" class="btn btn-danger"  onclick="deleteeventimage('<?php echo $image['image_id'];?>');"><i class="glyphicon glyphicon-remove-circle"></i></button>
</div>
<?php } 
}
?>
</div>

                       
                                <div class="panel-footer">                                   
                                    <input type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
          <script>
		  
		  /* function category_dispay(val)
{ 
if(val=="Department")
{
	document.getElementById('department').style.display="block";
	document.getElementById('college').style.display="none";
}
if(val=="College")
{
	document.getElementById('department').style.display="none";
	document.getElementById('college').style.display="block";
	
}
}*/
        function validate()
{ 

    var nicInstance = nicEditors.findEditor('content');
    var messageContent = nicInstance.getContent();

    if(messageContent=="<br>" || messageContent=="" ) { 
   // document.jvalidate.content.focus();
	
	document.getElementById('content-error').style.display = 'block';
     return false;
   }
   else {
        document.getElementById('content-error').style.display = 'none';
   }   
    return true;
} 
</script>    
