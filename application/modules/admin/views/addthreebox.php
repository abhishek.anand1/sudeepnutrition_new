<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#"> Three Box</a></li>
                    <li><a href="#">Edit</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/homethreebox/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong> Three Box</strong> Edit Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Box 1 Title </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_title1']; }?>" class="form-control" required="required" name="title1" id="title1"/>                                                                      
                                              
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Box 1 Link </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_link1']; }?>" class="form-control" required="required" name="link1" id="link1"/>                                                                      
                                              
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Box 1 Content </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                  <textarea class="form-control" rows="5" name="content1"><?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_content1']; }?></textarea>                                                                      
                                              
                                        </div>
                                    </div>
                                   
                                <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Box 1 Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image1"  id="file-simple1" />  Please uplaod an image with resolution 79 X 74      
                                                        </div>
                                     </div>
											
							
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Box 2 Title </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_title2']; }?>" class="form-control" required="required" name="title2" id="title2"/>                                                                      
                                              
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Box 2 Link </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_link2']; }?>" class="form-control" required="required" name="link2" id="link2"/>                                                                      
                                              
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Box 2 Content </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <textarea class="form-control" rows="5" name="content2"><?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_content2']; }?></textarea>                                                                        
                                              
                                        </div>
                                    </div>
                                     <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Box 2 Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image2"  id="file-simple2" />Please uplaod an image with resolution 79 X 74         
                                                        </div>
                                     </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Box 3 Title </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_title3']; }?>" class="form-control" required="required" name="title3" id="title3"/>                                                                      
                                              
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Box 3 Link </label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_link3']; }?>" class="form-control" required="required" name="link3" id="link3"/>                                                                      
                                              
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Box 3 Content </label>
                                        <div class="col-md-6 col-xs-12">  
                                          <textarea class="form-control" rows="5" name="content3"><?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_content3']; }?></textarea>                                          
                                                                                                                   
                                          <input type="hidden" value="<?php if(isset($list['threebox'])){ echo $list['threebox'][0]['homethreebox_id']; }?>" class="form-control" name="threeboxid" id="threeboxid"/>    
                                        </div>
                                    </div>
                                   
                                 <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Box 3 Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image3"  id="file-simple3" /> Please uplaod an image with resolution 79 X 74        
                                                        </div>
                                     </div>
													
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        