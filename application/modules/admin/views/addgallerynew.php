<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#"> Gallery</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/gallery/insertnew" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong> Gallery category</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                       
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Gallery Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['gallery'])){ echo $list['gallery'][0]['title']; }?>" class="form-control" name="title" id="title"/>                                                                      
                                                <input type="hidden" value="<?php if(isset($list['gallery'])){ echo $list['gallery'][0]['gallery_id']; }?>" class="form-control" name="galleryid" id="galleryid"/>   
                                                        
                                        </div>
                                    </div>
                              <?php /*?>  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Gallery Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                                                  
                                               <textarea class="form-control" rows="5" name="description"><?php if(isset($list['gallery'])){ echo $list['gallery'][0]['gallery_description']; }?></textarea>
                                        </div>
                                    </div>
                                </div><?php */?>
                                <?php if(isset($list['gallery'])){
									?>
                                <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Gallery Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image"  id="file-simple" />        
                                                        </div>
                                                    </div>
													<?php
								}
								else
								{
									?>
                                     <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Gallery Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image" required="required" id="file-simple" />        
                                                        </div>
                                                    </div>
                                    <?php
								}
								?>
													
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                                   <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                
                                <div class="panel-body">

                                <table id="datatable-responsive" class="table datatable table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>SI No</th>
                                     <th>Title</th>
                                    <th>Image</th>
                                    <th>Add images</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($list['imagelist'] as $val) {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                          <td><?php echo $val['title'];?></td>
                                        <td><img style="width:100px;heght:100px" src="<?php echo base_url()?>assets/gallery/<?php echo $val['image']?>" alt="<?php echo $val['gallery_id']?>" /></td>
                                        <td> <a href="<?php echo base_url('admin/gallery/image_gallery/'.$val['gallery_id']); ?>" class="btn btn-success"><i class="fa fa-plus"></i></a></td>
                                       
                                      <td><a href="<?php echo base_url('admin/gallery/editnew/'.$val['gallery_id']); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-edit"></i></a></td>
                                        <td><a href="<?php echo base_url('admin/gallery/deletenew/'.$val['gallery_id']); ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="glyphicon glyphicon-remove"></i></a></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>

                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                            
                            <!-- START DEFAULT TABLE EXPORT -->
                            
                            <!-- END DEFAULT TABLE EXPORT -->

                        </div>
                    </div>

                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        