<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#"> Gallery</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/gallery/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong> Gallery</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                       
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Gallery Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['gallery'])){ echo $list['gallery'][0]['gallery_title']; }?>" class="form-control" name="title" id="title"/>                                                                      
                                                <input type="hidden" value="<?php if(isset($list['gallery'])){ echo $list['gallery'][0]['gallery_id']; }?>" class="form-control" name="galleryid" id="galleryid"/>   
                                                     <input type="hidden" value="" class="form-control" name="description" id="description"/>      
                                        </div>
                                    </div>
                              <?php /*?>  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Gallery Description</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                                                                                  
                                               <textarea class="form-control" rows="5" name="description"><?php if(isset($list['gallery'])){ echo $list['gallery'][0]['gallery_description']; }?></textarea>
                                        </div>
                                    </div>
                                </div><?php */?>
                                <?php if(isset($list['gallery'])){
									?>
                                <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Gallery Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image"  id="file-simple" />        
                                                        </div>
                                                    </div>
													<?php
								}
								else
								{
									?>
                                     <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Gallery Image</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <input type="file" name="image" required="required" id="file-simple" />        
                                                        </div>
                                                    </div>
                                    <?php
								}
								?>
													
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        