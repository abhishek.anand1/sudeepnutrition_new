
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url()?>admin/dashboard">Home</a></li>
                    <li class="active">Media List</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Media List</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"></h3>
                                                                        
                                    
                                </div>
                                <div class="panel-body">
                                    <table id="customers2" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>SI No</th>
                                                <th>Title</th>
                                              <?php /*?>  <th>Description</th><?php */?>
                                                <th>Url</th>
                                                 <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
										$i=0;
										foreach($list['medias'] as $val)
										{
											$i++;
											?>
                                            <tr>
                                                <td><?php echo $i?></td>
                                                 <td><?php echo $val['media_title']?></td>
                                               <?php /*?> <td><?php echo $val['media_description']?></td><?php */?>
                                               
                                                <td><a href="<?php echo base_url()?>assets/media/<?php echo $val['media_image']?>" target="_blank"><?php echo base_url()?>assets/media/<?php echo $val['media_image']?></a></td>
                                                <td><a href="media/edit/<?php echo $val['media_id']?>" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i></a></td>
                                                <td><a href="media/delete/<?php echo $val['media_id']?> "  class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a></td>
                                                
                                            </tr>
                                            <?php
										}
										?>
                                        </tbody>
                                    </table>                                    
                                    
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                            
                            <!-- START DEFAULT TABLE EXPORT -->
                            
                            <!-- END DEFAULT TABLE EXPORT -->

                        </div>
                    </div>

                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->    

        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->        
        
        <!-- MESSAGE BOX-->
        
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
      






