<!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Academic Calendar</a></li>
                    <li><a href="#">Add</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/flashmessage/insert" enctype="multipart/form-data" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Academic Calendar</strong> Add Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['flash'])){ echo $list['flash'][0]['homeflashmessage_content']; }?>" class="form-control" name="message" required="required" id="message"/>                                                                      
                                        <input type="hidden" value="<?php if(isset($list['flash'])){ echo $list['flash'][0]['homeflashmessage_id']; }?>" class="form-control" name="flashid" id="flashid"/>            
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">URL</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                 <input type="text" value="<?php if(isset($list['flash'])){ echo $list['flash'][0]['homeflashmessage_url']; }?>" class="form-control" name="messageurl" required="required" id="messageurl"/>                                                                      
                                               
                                        </div>
                                    </div>
                                    </div>
                                <div class="panel-footer">                                   
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
                       
        