<?php
class Commonmodel extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_last_ten_entries()
    {
        $query = $this->db->get('entries', 10);
        return $query->result();
    }
	
	function select($condition=NULL,$table)
    {
        $this->db->select('*');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		$query =$this->db->get($table); 
        return $query->result_array();
    }
	function select_joinfieled($condition=NULL,$table,$join,$fields)
    {
        $this->db->select($fields);
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		foreach($join as $key=>$val)
		{
			$this->db->join($key,$val,'left');
		}
		$query =$this->db->get($table); 
		//echo $this->db->last_query();
        return $query->result_array();
    }
	function select_join($condition=NULL,$table,$join)
    {
        $this->db->select('*');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		foreach($join as $key=>$val)
		{
			$this->db->join($key,$val,'left');
		}
		$query =$this->db->get($table); 
		//echo $this->db->last_query();
        return $query->result_array();
    }
    function insert_entry($data,$table)
    {
         $this->db->insert($table,$data);
		 $insert_id = $this->db->insert_id();
   		return  $insert_id;
    }
	function delete_entry($table,$condition)
    {
        $this->db->where($condition);
		$this->db->delete($table); 

    }
    function update_entry($table,$condition,$val)
    {
        $this->db->update($table, $val, $condition);
    }
        function insertagain_entry($table,$condition,$val)
    {
         $this->db->insert($table, $val, $condition);
         $insert_id = $this->db->insert_id();
        return  $insert_id;
    }
    	function select_dec_asc($condition=NULL,$table,$desc=NULL,$asc=NULL)
    {
        $this->db->select('*');
		if($condition!=NULL)
		{
			$this->db->where($condition);
		}
		if($desc!=NULL)
		{
			$this->db->order_by($desc,'desc');
		}
		if($asc!=NULL)
		{
			$this->db->order_by($asc,'asc');
		}
		$query =$this->db->get($table); 
	
        return $query->result_array();
    }

}
