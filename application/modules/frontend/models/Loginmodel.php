<?php
class Loginmodel extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    
	function select($condition,$table)
    {
        $this->db->select('*');
		$this->db->where($condition);
		$query =$this->db->get($table); 
        return $query->result_array();
    }

   

}
