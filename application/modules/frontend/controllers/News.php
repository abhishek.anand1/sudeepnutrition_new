<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		$this->load->library('pagination');
		$config = array();
		$orderby='news_sortorder';
		$totlnews=$this->frontmodel->select(NULL,'tbl_news',NULL);
		$total_row=count($totlnews);
		$config["base_url"] = base_url()."news/";
		$limit=5;
		// Set total rows in the result set you are creating pagination for.
		$config["total_rows"] = $total_row;
		
		// Number of items you intend to show per page.
		$config["per_page"] =$limit;
		
		// Use pagination number for anchor URL.
		$config['use_page_numbers'] = TRUE;
		
		//Set that how many number of pages you want to view.
		$config['num_links'] = $total_row;
		
		// Open tag for CURRENT link.
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		
		// Close tag for CURRENT link.
		$config['cur_tag_close'] = '</a>';
		
		// By clicking on performing NEXT pagination.
		$config['next_link'] = '>';
		
		// By clicking on performing PREVIOUS pagination.
		$config['prev_link'] = '<';
		
		// To initialize "$config" array and set to pagination library.
		$this->pagination->initialize($config);
		
		
		// Create link.
		$str_links = $this->pagination->create_links();
		$result["links"] = explode('&nbsp;',$str_links );
		
		if($this->uri->segment(2)){
		 $page = ($this->uri->segment(2)) ;
		 $page =  $page-1;
		 $page=$limit* $page;
		}
		else{
		$page = 0;
		}
		$result['news']=$this->frontmodel->select_limit(NULL,'tbl_news',$page,$limit,$orderby);
		
		$result['page']="news";

		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
			$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
                	$breadcrumb="<span>".$result['page']."</span> ";
        $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
				$this->template->loadfront('template', 'news',$result);
		
	}
	public function details($id)
	{
		$condition=array('news_id'=>$id);
		$result['newsdeatils']=$this->frontmodel->select($condition,'tbl_news');
		$result['page']="news";
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
            	    $url=base_url().'news';
    				$breadcrumb="<a href='".$url."'>".'news'."</a> » ";
        $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
            $result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
		$this->template->loadfront('template', 'newsdetails',$result);
		
	}
	
	
	
}
