<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		$page_name=urldecode($this->uri->segment(2));
		$page_name=str_replace('_',' ',$page_name);
		//$newpage = str_replace('%20', ' ', $page);
		// $id=$_GET['id'];
		$condition=array('page_name'=>$page_name);
		if(isset($_GET['id']))
		$condition=array('page_id'=>$_GET['id']);
		$result['pagecontent']=$this->frontmodel->select($condition,'tbl_page');
        
		if(!$result['pagecontent'])
		redirect('home','refresh');
		// print_r($result['pagecontent']);die();
		$pageid=$result['pagecontent'][0]['page_id'];
		$newpage=$result['pagecontent'][0]['page_name'];
		$condition=array('pagetab_pageid'=>$pageid);
		$result['pagetab']=$this->frontmodel->select($condition,'tbl_pagetab');
		$parentname="";
		$result['fisrsparentpage']="";
		if($result['pagecontent'])
		{
				$condition=array('page_parent'=>$pageid);
				$result['innerpages']=$this->frontmodel->select($condition,'tbl_page');
				$parentid=$result['pagecontent'][0]['page_parent'];
				$breadcrumb="<span >".$newpage."</span>";
				while($parentid!=0)
				{
					$condition=array('page_id'=>$parentid);
					$parandetails=$this->frontmodel->select($condition,'tbl_page');
					$parentname=$parandetails[0]['page_name'];
					$parentid=$parandetails[0]['page_parent'];
					$childid=$parandetails[0]['page_id'];
					$url=base_url()."page/".str_replace(' ','_',$parentname);
					$breadcrumb="<a href='".$url."'>".$parentname."</a> » ".$breadcrumb;
					if($parentid!=0)
					{
						$result['fisrsparentpage']=$parentname;
					}
				}
				$result['parentpage']=$parentname;
		}
		
		$result['page']=$newpage;
		$result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
		$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
        //var_dump($result); exit;
		$this->template->loadfront('template', 'page',$result);
		
	}
	
	
	
}
