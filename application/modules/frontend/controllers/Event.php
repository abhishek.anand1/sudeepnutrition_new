<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		$breadcrumb="<span>".'events'."</span> ";
		$this->load->library('pagination');
		if($this->uri->segment(2)=='college' && $this->uri->segment(3)){
			$college_name=urldecode($this->uri->segment(3));
		    $college_name=str_replace('_',' ',$college_name);
			$condition1=array('college_name'=>$college_name);
			$data=$this->frontmodel->select($condition1,'tbl_college',NULL);
			$condition=array('events_collegeid'=>$data[0]['college_id']);
		}elseif($this->uri->segment(2)=='department' && $this->uri->segment(3)){
			$college_name=urldecode($this->uri->segment(3));
		    $college_name=str_replace('_',' ',$college_name);
			$condition1=array('department_name'=>$college_name);
			$data=$this->frontmodel->select($condition1,'tbl_department',NULL);
			$condition=array('events_departmentid'=>$data[0]['department_id']);
            $cond=array('college_id'=>$data[0]['department_collegeid']);
			$da=$this->frontmodel->select($cond,'tbl_college',NULL);
            $det=$da[0];
            $url=base_url()."event/college/".str_replace(' ', '_', $det['college_name']);
            $breadcrumb="<a href='".$url."'>".$det['college_name']."</a> » ".$breadcrumb;
		}
		else if(isset($_GET['department']))
		{
			$condition=array('events_departmentid'=>$_GET['department']);
		}
		else
		{
			$condition=array('events_collegeid'=>$_GET['college']);
		}
		$config = array();
		$desc='events_date';
		$asc='events_sortorder';
		//$result['events']=$this->frontmodel->select_dec_asc(NULL,'tbl_events',$desc,$asc);
		$totlevents=$this->frontmodel->select($condition,'tbl_events',NULL);
		$total_row=count($totlevents);
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		
		$config["base_url"] = base_url()."event/".$this->uri->segment(2)."/".$this->uri->segment(3);
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$limit=10;
		// die($config["base_url"]);
		// Set total rows in the result set you are creating pagination for.
		$config["total_rows"] = $total_row;
		
		// Number of items you intend to show per page.
		$config["per_page"] =$limit;
		
		// Use pagination number for anchor URL.
		$config['use_page_numbers'] = TRUE;
		
		//Set that how many number of pages you want to view.
		$config['num_links'] =5;
		//$config['num_links'] = $total_row;
		
		// Open tag for CURRENT link.
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		
		// Close tag for CURRENT link.
		$config['cur_tag_close'] = '</a>';
		
		// By clicking on performing NEXT pagination.
		$config['next_link'] = FALSE;

		$config['prev_link'] = FALSE;
		
		//$config['first_link']    =   '<<';
		if(ceil($config['total_rows']/$config['per_page'])> 5)
				$config['last_link']    =   '.... >>';
			else 
				$config['last_link']    =   '>>';
				
		if(ceil($config['total_rows']/$config['per_page'])> 5)
				$config['first_link']    =   '<< ....';
			else 
				$config['first_link']    =   '<<';
		// To initialize "$config" array and set to pagination library.
		$this->pagination->initialize($config);
		// Create link.
		$str_links = $this->pagination->create_links();
		$result["links"] = explode('&nbsp;',$str_links );
		
		if($this->uri->segment(4)){
		 $page = ($this->uri->segment(4)) ;
		 $page =  $page-1;
		 $page=$limit* $page;
		}
		else{
		$page = 0;
		}
		$result['events']=$this->frontmodel->select_limit($condition,'tbl_events',$page,$limit,$asc);
		
		$result['page']="events";
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
		$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);


		if(isset($_GET['college']))
		{
			$condition=array('department_collegeid'=>$_GET['college']);
			$result['underdepartment']=$this->frontmodel->select($condition,'tbl_department',NULL);
		}
		else
		{
			$result['underdepartment']=array();
		}
		
        $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
		
		$this->template->loadfront('template', 'events',$result);
		
	}
	public function details($id)
	{
		$condition=array('events_id'=>$id);
       $dd = $this->frontmodel->select($condition,'tbl_events');
       $dd=$dd[0];
       $join=array();
       if($dd['events_collegeid']){
            $join["tbl_college"]="tbl_events.events_collegeid=tbl_college.college_id";
       }
         if($dd['events_departmentid']){
             $join["tbl_department"]="tbl_events.events_departmentid=tbl_department.department_id";
         }
  
        $details=$this->frontmodel->select_join_order_desc($condition,'tbl_events',$join,'events_date');
    	$result['eventsdeatils']=$details;
    //   print_r($details);die();
        $details=$details[0];
        $breadcrumb="<span >".'event'."</span>";
         if($details['events_departmentid']){
                   
                     $url=base_url()."event/department/".str_replace(' ', '_', $details['department_name']);
                    $breadcrumb="<a href='".$url."'>".$details['department_name']."</a> » ".$breadcrumb;
                }
                if($details['events_collegeid']){
                     
                    $url=base_url()."event/college/".str_replace(' ', '_', $details['college_name']);
                    $breadcrumb="<a href='".$url."'>".$details['college_name']."</a> » ".$breadcrumb;
                }
               
	//	$result['eventsdeatils']=$this->frontmodel->select($condition,'tbl_events');
		$result['page']="events";
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
		$condition=array('event_id'=>$id);
		$result['images']=$this->frontmodel->select($condition,'tbl_eventimages',NULL);
		$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
     //   $breadcrumb="<span>".$result['page']."</span> ";
        $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
		$this->template->loadfront('template', 'eventsdetails',$result);
	}
	public function academic_calendar() {
		$result['page']="academic_calendar";
		$breadcrumb="<span >".'academic calendar'."</span>";
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
		$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
		$result['calendar']=$this->frontmodel->select(NULL,'tbl_homeflashmessage',NULL);
        $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
		$this->template->loadfront('template', 'academic-calendar',$result);
	}
	
	
}
