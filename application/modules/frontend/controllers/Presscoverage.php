<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presscoverage extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		$orderby='press_date';
		$result['press']=$this->frontmodel->select_orderdesc(NULL,'tbl_press',$orderby);
		$result['page']="press";
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
			$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
                	$breadcrumb="<span>".$result['page']."</span> ";
        $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
		$this->template->loadfront('template', 'press',$result);
		
	}
	
	
	
}
