﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
@ini_set('display_errors', 0);
class Contact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {

            parent::__construct();
            //print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		$result['contactaddress']=$this->frontmodel->select(NULL,'tbl_contactaddress');
		$result['page']="contact";
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
                $breadcrumb="<span>".$result['page']."</span> ";
                $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
        $result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
		$this->template->loadfront('template', 'contact-us',$result);
		
	}
	
	public function quickcontact()
	{
		require $_SERVER["DOCUMENT_ROOT"].'/PHPMailer/PHPMailerAutoload.php';
			if(isset($_POST['email'])) 
			{
				$name = $_POST['name']; // required
				//$subject = $_POST['subject'];
				$email = $_POST['email']; // required
				$phone = $_POST['phone']; // required
				$message= $_POST['message']; // required
				//$page= $_POST['page']; // required
				
							$to = "info@tjohngroup.com,azhar@codelattice.com";
							$subject = "Quick Contact";

							$message = '<table border="0" cellspacing="0" cellpadding="10" style="max-width:450px; border:1px solid #eee; width:100%">
							
							  <tr>
								<td style="border-bottom:1px solid #eee;">Name</td>
								<td style="border-bottom:1px solid #eee;">'.$name.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Email</td>
								<td style="border-bottom:1px solid #eee;">'.$email.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Phone</td>
								<td style="border-bottom:1px solid #eee;">'.$phone.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Message</td>
								<td style="border-bottom:1px solid #eee;">'.$message.'</td>
							  </tr>
							</table>';

							// Always set content-type when sending HTML email
							$headers = "MIME-Version: 1.0" . "\r\n";
							$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

							// More headers
							$headers .= 'From: noreply2cl@gmail.com' . "\r\n";

							mail($to,$subject,$message,$headers);



				// $mail = new PHPMailer;
				
				// //Enable SMTP debugging. 
				// //$mail->SMTPDebug = 3;                               
				// //Set PHPMailer to use SMTP.
				// //$mail->isSMTP();            
				// //Set SMTP host name                          
				// $mail->Host = "smtp.gmail.com";
				// //Set this to true if SMTP host requires authentication to send email
				// $mail->SMTPAuth = true;                          
				// //Provide username and password     
				// $mail->Username = "noreply2cl@gmail.com";                 
				// $mail->Password = "clt-cl@admin14";                           
				// //If SMTP requires TLS encryption then set it
				// $mail->SMTPSecure = "tls";                           
				// //Set TCP port to connect to 
				// $mail->Port = 587;                                   
				
				// $mail->From = "noreply2cl@gmail.com";
				// $mail->FromName = "T.John College";
				
				// $mail->addAddress("info@tjohngroup.com", "Info");
				
				// $mail->isHTML(true);
				
				// $mail->Subject = "Quick Contact";
				
				$name = $_POST['name']; // required
				// //$subject = $_POST['subject'];
				$email = $_POST['email']; // required
				$phone = $_POST['phone']; // required
				$message= $_POST['message']; // required
				$page= $_POST['page']; // required
				// $mail->Body='';
				// $mail->AltBody = "This is the plain text version of the email content";

				$enqdate=date('Y-m-d');
				$link=$_SERVER['HTTP_REFERER'];
				$link_array = explode('/',$link);
    			$frompage = end($link_array);
				$frompage = str_replace('%20', ' ', $frompage);
				$data=array("enquiries_name"=>$name,"enquiries_email"=>$email,"enquiries_phone"=>$phone,"enquiries_message"=>$message,"enquiries_category"=>"Quick 			Contact","enquiries_date"=>$enqdate,'enquiries_frompage'=>$frompage);
				 $this->frontmodel->insert_entry($data,'tbl_enquiries');
				
				
			 if(0) 
				{
					 $this->session->set_flashdata('message', 'Sending faild');
					  redirect($_SERVER['HTTP_REFERER']);
				} 
				else 
				{
					
					 $this->session->set_flashdata('message', 'Succesfully sent your message');
					 redirect($_SERVER['HTTP_REFERER']);
					
				}
			
			}
	}
	
	public function send()
	{
	       require $_SERVER["DOCUMENT_ROOT"].'/PHPMailer/PHPMailerAutoload.php';
	if(isset($_POST['email'])) {
		$name = $_POST['name']; // required
		$phone = $_POST['phone']; // required
		$subject = $_POST['subject']; // required
	
		$email_from = $_POST['email']; // required
		
		$message= $_POST['message']; // required
		$to = "info@tjohngroup.com,azhar@codelattice.com";
							$subject = "Contact Enquiry";

							$message = '<table border="0" cellspacing="0" cellpadding="10" style="max-width:450px; border:1px solid #eee; width:100%">
							
							  <tr>
								<td style="border-bottom:1px solid #eee;">Name</td>
								<td style="border-bottom:1px solid #eee;">'.$name.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Email</td>
								<td style="border-bottom:1px solid #eee;">'.$email_from.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Phone</td>
								<td style="border-bottom:1px solid #eee;">'.$phone.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Message</td>
								<td style="border-bottom:1px solid #eee;">'.$message.'</td>
							  </tr>
							</table>';

							// Always set content-type when sending HTML email
							$headers = "MIME-Version: 1.0" . "\r\n";
							$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

							// More headers
							$headers .= 'From: noreply2cl@gmail.com' . "\r\n";

							mail($to,$subject,$message,$headers);
		// $mail = new PHPMailer;
		
		// //Enable SMTP debugging. 
		// //$mail->SMTPDebug = 3;                               
		// //Set PHPMailer to use SMTP.
		// //$mail->isSMTP();            
		// //Set SMTP host name                          
		// $mail->Host = "smtp.gmail.com";
		// //Set this to true if SMTP host requires authentication to send email
		// $mail->SMTPAuth = true;                          
		// //Provide username and password     
		// $mail->Username = "noreply2cl@gmail.com";                 
		// $mail->Password = "clt-cl@admin14";                           
		// //If SMTP requires TLS encryption then set it
		// $mail->SMTPSecure = "tls";                           
		// //Set TCP port to connect to 
		// $mail->Port = 587;                                   
		
		// $mail->From = "noreply2smartrozger@gmail.com";
		// $mail->FromName = "T.John College";
		// $mail->addAddress("info@tjohngroup.com", "Info");
		// //$mail->addAddress("vivek@codelattice.com", "vivek");
		// //$mail->addAddress("vivek.kjk@gmail.com", "vivek");
		// //$mail->addAddress("harif@codelattice.com", "Harif");
		
		// $mail->isHTML(true);
		
		// $mail->Subject = "Contact Enquiry";
		
		$name = $_POST['name']; // required
		$phone = $_POST['phone']; // required
		$subject = $_POST['subject']; // required
	
		$email_from = $_POST['email']; // required
		
		$message= $_POST['message']; // required
				// $mail->AltBody = "This is the plain text version of the email content";
		$enqdate=date("d/m/Y");
				$data=array("enquiries_name"=>$name,"enquiries_email"=>$email_from,"enquiries_phone"=>$phone,"enquiries_message"=>$message,"enquiries_category"=>"Contact Us","enquiries_date"=>$enqdate,"enquiries_suject"=>$subject);
				 $this->frontmodel->insert_entry($data,'tbl_enquiries');
		if(0) 
		{
			 $this->session->set_flashdata('message', 'Sending faild');
			  redirect(base_url().'frontend/contact');
		} 
		else 
		{
			 $this->session->set_flashdata('message', 'Succesfully sent your message');
			 redirect(base_url().'frontend/contact');
			
		}
	
			
		}
	}
	
	
}?>