<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
		// $result['gallery']=$this->commonmodel->select(NULL,'tbl_gallery');
		// $result['page']="gallery";
		// $result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		// $result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
		// 	$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
  //               	$breadcrumb="<span>".$result['page']."</span> ";
  //       $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
		// $this->template->loadfront('template', 'gallery',$result);
		$result['gallery']=$this->commonmodel->select(NULL,'tbl_gallerynew');
		$result['page']="album";
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
			$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
                	$breadcrumb="<span>".$result['page']."</span> ";
        $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
		$this->template->loadfront('template', 'album',$result);
		
	}
	
		public function category()
	{
		
		$result['gallery']=$this->commonmodel->select(NULL,'tbl_gallerynew');
		$result['page']="gallerynew";
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
			$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
                	$breadcrumb="<span>".$result['page']."</span> ";
        $result['breadcrumb']="<a href='".base_url()."'>Home</a> » ".$breadcrumb;
		$this->template->loadfront('template', 'gallerynew',$result);
		
	}

	public function details($id)
	{
		 $condition=array('gallery_id'=>$id);
		$result['gallery']=$this->frontmodel->select_orderdesc($condition,'tbl_galleryfull','galleryfull_id');
		$data=$result['gallery'][0];
		//print_r($data['title']);die();
		$result['page']="gallery";
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
			$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
                	$breadcrumb="<span>".$result['page']."</span> ";
        $result['breadcrumb']="<a href='".base_url()."'>Home</a> » <a href='".base_url()."/gallery'>Album</a> » <a href='".base_url()."/gallery'>".$data['title']."</a> »  ".$breadcrumb;
		$this->template->loadfront('template', 'gallery',$result);
		
	}
	
}
