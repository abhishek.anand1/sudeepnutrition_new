<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Footerlinks extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index($page)
	{
		$newpage = str_replace('%20', ' ', $page);
		if($newpage=="NSS")
		{
			$result['page']="NSS Cell";
			$result['pagecontent']=$this->frontmodel->select(NULL,'tbl_nss');
		}
		if($newpage=="ISO")
		{
			$result['page']="ISO 9001:2008";
			
			$result['pagecontent']=$this->frontmodel->select(NULL,'tbl_iso');
		}
		if($newpage=="LWO")
		{
			$result['page']="Library Web OPAC";
			$result['pagecontent']=$this->frontmodel->select(NULL,'tbl_lwo');
		}
		if($newpage=="IQAC")
		{
			$result['page']="Internal Quality Assurance Cell (IQAC)";
			$result['pagecontent']=$this->frontmodel->select(NULL,'tbl_iqac');
		}
		if($newpage=="Digital Library")
		{
			$result['page']="TJIT Digital Library";
			$result['pagecontent']=$this->frontmodel->select(NULL,'tbl_digitallibrary');
		}
			if($newpage=="stafflinks")
		{
			$result['page']="Staff Links";
			$result['pagecontent']=$this->frontmodel->select(NULL,'tbl_stafflinks');
		}
		//print_r($result['pagecontent']);
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select(NULL,'tbl_department',NULL);
		$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);

		$this->template->loadfront('template', 'footerpage',$result);
		
		
	}
	
	
	
}
