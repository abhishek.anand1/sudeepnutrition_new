<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			
			
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
	
		//$result['slider']=$this->commonmodel->select(NULL,'tbl_homeslider');
		
	//	$join=array("tbl_page"=>"tbl_page.page_id=tbl_homeslider.homeslider_page");
		$result['slider']=$this->frontmodel->select(NULL,'tbl_homeslider');
		$result['threebox']=$this->frontmodel->select(NULL,'tbl_homethreebox');
		$result['flashmessage']=$this->frontmodel->select(NULL,'tbl_homeflashmessage');
		$result['placements']=$this->frontmodel->select(NULL,'tbl_placements');
		$result['about']=$this->frontmodel->select(NULL,'tbl_homeabout');
		$newcondition=array('news_featured'=>1);
		$result['news']=$this->frontmodel->select($newcondition,'tbl_news');
		$result['events']=$this->frontmodel->select(NULL,'tbl_events');
		$result['page']="home";
		$dept_condition=array('show_in_event'=>1);
		$result['college']=$this->frontmodel->select(NULL,'tbl_college',NULL);
		$result['department']=$this->frontmodel->select($dept_condition,'tbl_department',NULL);
			$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
			$result['hostel']=$this->commonmodel->select(NULL,'tbl_homehostel');
		$this->template->loadfront('template', 'home',$result);
		
	}
		/*public function index()
	{
		
		$data['list']['slider']=$this->commonmodel->select(NULL,'tbl_homeslider');
		$data['list']['threebox']=$this->commonmodel->select(NULL,'tbl_homethreebox');
		$data['list']['flashmessage']=$this->commonmodel->select(NULL,'tbl_homeflashmessage');
		$data['list']['placements']=$this->commonmodel->select(NULL,'tbl_placements');
		$data['list']['about']=$this->commonmodel->select(NULL,'tbl_homeabout');
		$newcondition=array('news_featured'=>1);
		$data['list']['news']=$this->commonmodel->select($newcondition,'tbl_news');
		$data['list']['events']=$this->commonmodel->select(NULL,'tbl_events');
		$data['list']['page']="home";
		$data['list']['contactadress'] = $this->frontmodel->select(NULL,'tbl_contactaddress');
				
		$data['list']['menus'] = $this->frontmodel->select_menu(NULL,'tbl_menu');
				
		$pagecondition=array('page_showinmenu'=>1);
		$data['list']['pages'] = $this->frontmodel->select($pagecondition,'tbl_page');
		//$this->template->loadfront('template', 'home',$result);
		$this->load->view('home',$data);
		
	}*/
	
}
