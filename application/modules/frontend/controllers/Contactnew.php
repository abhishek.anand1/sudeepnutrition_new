<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactnew extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	   public function __construct()
       {
            parent::__construct();
			//print_r($result['adminuser']);
            // Your own constructor code
       }
	public function index()
	{
		
//		$result['contactaddress']=$this->commonmodel->select(NULL,'tbl_contactaddress');
		$result['page']="contact";
		$result['link']=$this->frontmodel->select(NULL,'tbl_footerlinks',NULL);
		$this->template->loadfront('template', 'contact',$result);
		
	}
	public function quickcontact()
	{
		
        $secret = '6Ldp1BsUAAAAAMcTj7L_hpKP7e-GLSTjRtdWQGu9';
       
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);

        if($responseData->success)
        {
		$name=$this->input->post('name');
		$phone=$this->input->post('phone');
		$email=$this->input->post('email');
		$message=$this->input->post('message');
		$to = "info@tjohngroup.com";
		$subject = "Quick Contact";
                $message = '<table border="0" cellspacing="0" cellpadding="10" style="max-width:450px; border:1px solid #eee; width:100%">
							
							  <tr>
								<td style="border-bottom:1px solid #eee;">Name</td>
								<td style="border-bottom:1px solid #eee;">'.$name.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Email</td>
								<td style="border-bottom:1px solid #eee;">'.$email.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Phone</td>
								<td style="border-bottom:1px solid #eee;">'.$phone.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Message</td>
								<td style="border-bottom:1px solid #eee;">'.$message.'</td>
							  </tr>
							</table>';
	$headers = "MIME-Version: 1.0" . "\r\n";
							$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $enqdate=date('Y-m-d');
	$link=$_SERVER['HTTP_REFERER'];
	$link_array = explode('/',$link);
    $frompage = end($link_array);
	$frompage = str_replace('%20', ' ', $frompage);
    $message1=$this->input->post('message');
	$data=array("enquiries_name"=>$name,"enquiries_email"=>$email,"enquiries_phone"=>$phone,"enquiries_message"=>$message1,"enquiries_category"=>"Quick 			Contact","enquiries_date"=>$enqdate,'enquiries_frompage'=>$frompage);
	$this->frontmodel->insert_entry($data,'tbl_enquiries');
				
	if(mail($to, $subject, $message, $headers))
	{
	 $this->session->set_flashdata('message', 'Succesfully sent your message');
         
	}
	else
	{
	 $this->session->set_flashdata('message', 'Sending faild');
				
	}
	}else{
	 $this->session->set_flashdata('message', 'Capthca Failed');
			
    }
	redirect($_SERVER['HTTP_REFERER']);
		
	}
        public function send()
	{
		
        $secret = '6Ldp1BsUAAAAAMcTj7L_hpKP7e-GLSTjRtdWQGu9';
       
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);

        if($responseData->success)
        {
		$name=$this->input->post('name');
		$phone=$this->input->post('phone');
		$email=$this->input->post('email');
		$message=$this->input->post('message');
		$to = "info@tjohngroup.com";
		$subject = "Contact Enquiry";
                $message = '<table border="0" cellspacing="0" cellpadding="10" style="max-width:450px; border:1px solid #eee; width:100%">
							
							  <tr>
								<td style="border-bottom:1px solid #eee;">Name</td>
								<td style="border-bottom:1px solid #eee;">'.$name.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Email</td>
								<td style="border-bottom:1px solid #eee;">'.$email.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Phone</td>
								<td style="border-bottom:1px solid #eee;">'.$phone.'</td>
							  </tr>
							  <tr>
								<td style="border-bottom:1px solid #eee;">Message</td>
								<td style="border-bottom:1px solid #eee;">'.$message.'</td>
							  </tr>
							</table>';
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    $enqdate=date('Y-m-d');
	$link=$_SERVER['HTTP_REFERER'];
	$link_array = explode('/',$link);
    $frompage = end($link_array);
	$frompage = str_replace('%20', ' ', $frompage);
    $message1=$this->input->post('message');
	$data=array("enquiries_name"=>$name,"enquiries_email"=>$email,"enquiries_phone"=>$phone,"enquiries_message"=>$message1,"enquiries_category"=>"Contact Us","enquiries_date"=>$enqdate,"enquiries_suject"=>$subject);
	$this->frontmodel->insert_entry($data,'tbl_enquiries');

				
	if(mail($to, $subject, $message, $headers))
	{
	 $this->session->set_flashdata('message', 'Succesfully sent your message');
         
	}
	else
	{
	 $this->session->set_flashdata('message', 'Sending faild');
				
	}
}else{
	 $this->session->set_flashdata('message', 'Capthca Failed');
			
}
	redirect($_SERVER['HTTP_REFERER']);
		
	}

}
