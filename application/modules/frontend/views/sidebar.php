<h3 class="sidebar-head">Quick Connect</h3>
<style>
.g-recaptcha {
    transform: scale(0.77);
    transform-origin: 0 0;
}
</style>
<div class="quick-contact">
	<form method="post" id="quick-contact" action="<?php echo base_url()?>contact_new/quickcontact">
		<fieldset class="form-group">
			<input type="text" class="form-control" name="name" placeholder="NAME*" />
		</fieldset>
		<fieldset class="form-group">
			<input type="text" class="form-control" name="email" placeholder="EMAIL*" />
		</fieldset>
		<fieldset class="form-group">
			<input type="text" class="form-control" name="phone" placeholder="PHONE*" />
		</fieldset>
		<fieldset class="form-group">
			<textarea rows="3" cols="30" placeholder="MESSAGE (Not exceed 80 character)" name="message" class="form-control" maxlength="80"></textarea>
		</fieldset>
		<fieldset class="form-group text-right">
                                        <script src='https://www.google.com/recaptcha/api.js'></script>
                                        <div class="g-recaptcha" data-sitekey="6Ldp1BsUAAAAAPSxj7j-245mAYHlq2f-4NSMlqS0"></div>
			<input type="submit" class="btn" value="Submit" />
		</fieldset>
        <?php
					if($this->session->flashdata('message'))
					{
						?>
                                <p style="color:#090;"><?php echo $this->session->flashdata('message');?></p>

                            <?php
					}
					?>
	</form>
</div>
<div class="placement-container">
	
	<div class="placement-frame">
    <h3>Placements of the year</h3>
    <div class="plcementslider">
        <?php
		foreach($list['placements'] as $val)
		{
			?>
    <li>
		<img src="<?php echo base_url()?>assets/placementsprofile/<?php echo $val['placements_candidateimage']?>" width="260" height="260">
		<p class="name"><?php echo $val['placements_candidatename']?></p>
		<p><?php echo $val['placements_description']?><p>
		<p class="company"><?php echo $val['placements_company']?></p>
        </li>
        <?php
		}
		?>
        </div>
	</div>
</div>
