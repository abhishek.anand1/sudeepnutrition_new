<style>
.main-content ul {
    padding-left: 17px;
}
.main-content li {
    list-style: initial;
}
.thirdmenu li {
    list-style: none;
}
.main-content table td {
    border:1px solid #000;
	padding:10px;
}
</style>
<div class="container margin-top-10">
	<div class="row">
		<div class="col-xs-12 col-md-8 col-lg-9">
			<div class="inner-banner" style="padding-bottom: 0px;">
				<?php if( $list['pagecontent']){if( $list['pagecontent'][0]['page_banner']){?><img src="<?php echo base_url()?>assets/homeslider/<?php echo $list['pagecontent'][0]['page_banner']?>" width="750" height="350" alt="T John College">
                <?php
				}}
				?>
			</div>
            <div class="breadcrumb">
            	<?php echo $list['breadcrumb'];?>  
     		</div>
			<div class="main-content">
            <?php 
			
			if(count($list['innerpages'])>0)
			{
				?>
                    <div class="thirdmenu">   
                         <div class="head">
                         	<?php /*?><h6><?php echo $list['page'];?></h6><?php */?>
                            <h6>Departments</h6>
                         </div>
                          <ul>
                          <?php foreach($list['innerpages'] as $innerpage)
						  {
							  ?>
                               <li><a title="FLEXCUBE" href="<?php echo NEW_URL;?>page/<?php echo str_replace(' ','_',$innerpage['page_name']);?>"><?php echo $innerpage['page_name'];?></a></li>
                            <?php
						  }
						  ?>
                           </ul>
                  	</div>
                    <?php
			}
			?>
                    <div>
                        <h3> <h2><?php echo $list['page'];?></h2></h3>
                        
                        <?php if($list['pagecontent']){ echo $list['pagecontent'][0]['page_content']; }?>
                	</div>
                    
                    <?php 
					
					if(count($list['pagetab'])>0)
					{
						?>
                    <div id="horizontalTab">
        <ul>
        <?php foreach($list['pagetab'] as $tabpage)
						  {
							  ?>
            <li><a href="#<?php echo $tabpage['pagetab_id'];?>"><?php echo $tabpage['pagetab_title'];?></a></li>
           <?php
						  }
						  ?>
        </ul>
 <?php foreach($list['pagetab'] as $tabpage)
						  {
					 ?>
						<div id="<?php echo $tabpage['pagetab_id'];?>">
							<p><?php echo $tabpage['pagetab_content'];?></p>
						</div>
       
       <?php
						  }
						  ?>
    </div>
    <?php
					}
					?>
			</div>
            
		</div>
		<div class="col-xs-12 col-md-4 col-lg-3">
        <?php if(@$list['pagecontent'][0]['page_hassidemenu']==1){?>
			<?php include 'sidebar.php'; ?>
            <?php
		}
		?>
		</div>
	</div>
</div>
