<!DOCTYPE html>
<html lang="en">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-54VNK9T');</script>
<!-- End Google Tag Manager -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1301988366649620');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1301988366649620&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
    <meta name="google-site-verification" content="OIv7pfkKCwQFeCwiIzhYuICLu_6-G-NJ1d9QC4QhWzg" />
    <meta charset="utf-8">
	<meta name="robots" content="noarchive">
<meta Http-Equiv="Cache" content="no-cache">
<meta Http-Equiv="Pragma-Control" content="no-cache">
<meta Http-Equiv="Cache-directive" Content="no-cache">
<meta Http-Equiv="Pragma-directive" Content="no-cache">
<meta Http-Equiv="Cache-Control" Content="no-cache">
<meta Http-Equiv="Pragma" Content="no-cache">
<meta Http-Equiv="Expires" Content="0">
<meta Http-Equiv="Pragma-directive: no-cache">
<meta Http-Equiv="Cache-directive: no-cache">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="T John Group of Institutions consisting of dynamic & internationally recognized colleges with a long established tradition in providing excellence in education, has dramatically expanded the under graduate & post graduate programmes to offer degrees of demand such as Science, Management, Technology, Hospitality, computers, etc . ."/>
	<link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico" type="image/x-icon" />
	<title>T John College</title>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,500' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<!-- Important Owl stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.carousel.css">
	<!-- Default Theme -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.theme.css">
	<link href="<?php echo base_url()?>assets/css/tooltip_skin_variation.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo base_url()?>assets/css/dcalendar.picker.css" rel="stylesheet">
	<link href="<?php echo base_url()?>assets/css/layout.css" rel="stylesheet">
     <link rel="stylesheet" href="<?php echo base_url()?>assets/css/calendar.css">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/lightview.css" />
     <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/responsive-tabs.css" />
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--old of Zopim Live Chat Script-->
<!--<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?45n8DH5mSX8OIoDjz1CYwdyI63cLEaMC";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>-->
<!--End of old Zopim Live Chat Script-->
<!--Start of Zendesk Chat Script-->
<script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-104452898-1', 'auto');  ga('send', 'pageview');
</script>
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4WqDWi3D6VPZZGLoGXpC3fH0uLxSty0v";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

</head>

<body id="top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-54VNK9T"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<header>
		<div class="container">
			<div class="logo">
				<a href="<?php echo NEW_URL; ?>">
					<img src="<?php echo base_url()?>assets/images/logo.jpg">
				</a>
			</div>
			<div class="qc-holder">
				<div class="phone-no"><a href="tel:<?php echo @$list['contactadress'][0]['contactaddress_phone'];?>"><?php echo @$list['contactadress'][0]['contactaddress_phone'];?></a>
				</div>
				<div class="email"><a href="mailto:<?php echo @$list['contactadress'][0]['contactaddress_email'];?>"><?php echo @$list['contactadress'][0]['contactaddress_email'];?></a>
				</div>
				<div class="phone-icon"><a href="tel:<?php echo @$list['contactadress'][0]['contactaddress_phone'];?>"><i class="fa fa-phone" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
        <?php 
		if(!isset($list['fisrsparentpage']))
		{
			$list['fisrsparentpage']="";
		}
		if(!isset($list['parentpage']))
		{
			$list['parentpage']="";
		}
		?>
		<div class="main-menu">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> <span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
                   
					<div class="collapse navbar-collapse" id="example-navbar-collapse">
						<ul class="nav navbar-nav">
							<li <?php if($list['page']=='home'){?> class="active" <?php } ?>><a href="<?php echo NEW_URL; ?>">Home</a>
							</li>
                            <?php
							foreach($list['menus'] as $menuval)
							{
								$levelonecount=0;
								foreach($list['pages'] as $pageval)
								{
									if($pageval['page_parent']== $menuval['menu_page_id'])
									{
										$levelonecount++;
									}
								}
								if($levelonecount>0)
								{
									?>
                                    <li <?php if($list['page']==$menuval['page_name'] || $list['parentpage']==$menuval['page_name'] ){?> class="active dropdown" <?php } else {?>  class="dropdown" <?php } ?>><a class="dropdown-toggle" data-toggle="dropdown" href="<?php if($menuval['link']) echo NEW_URL.'page/'.str_replace(' ','_',$menuval['page_name']); else '#';?>"> <?php echo $menuval['page_name']?> <b class = "caret"></b></a>
                                        <ul class="dropdown-menu">
                                        <?php
                                        
                                        foreach($list['pages'] as $pageval)
                                        {
                                            if($pageval['page_parent']== $menuval['menu_page_id'])
                                            {
												$leveltwocount=0;
												foreach($list['pages'] as $pageval2)
												{
													if($pageval['page_id']== $pageval2['page_parent'])
													{
														$leveltwocount++;
													}
												}
                                                if($leveltwocount>0)
                                                {
                                                ?>
                                                    <li <?php if($list['page']==$pageval['page_name'] || $list['fisrsparentpage']==$pageval['page_name'] ){?> class="active" <?php } ?>><a href="<?php echo NEW_URL.'page/'.str_replace(' ','_',$pageval['page_name']);?>"><?php echo $pageval['page_name']?> <span class="caret"></span></a>
                                                        <ul class="dropdown-menu">
                                                        <?php
                                                        foreach($list['pages'] as $pageval3)
                                                        {
                                                            if($pageval3['page_parent']== $pageval['page_id'])
                                                            {
                                                             ?>
                                                           		 <li <?php if($list['page']==$pageval3['page_name']){?> class="active" <?php } ?>><a href="<?php echo NEW_URL.'page/'.str_replace(' ','_',$pageval3['page_name']);?>"><?php echo $pageval3['page_name']?> </a></li>
                                                            <?php
                                                            }
                                                        }
                                                            ?>
                                                            
                                                        </ul>
                                                    </li>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                <li <?php if($list['page']==$pageval['page_name']){?> class="active" <?php } ?>><a href="<?php echo NEW_URL.'page/'.str_replace(' ','_',$pageval['page_name']);?>"><?php echo $pageval['page_name']?></a></li>
                                                <?php
                                                }
                                            }
                                        }
                                            ?>
                                        </ul>
                                    </li>
                            <?php
								 }
								 else
								 {
									 ?>
                                     <li <?php if($list['page']==$menuval['page_name']){?> class="active" <?php } ?>><a href="<?php echo NEW_URL.'page/'.str_replace(' ','_',$menuval['page_name']);?>" ><?php echo $menuval['page_name']?></a>
                                     <?php
								 }
								 ?>
							
							<?php
							}
							?>
                            <li><a href="http://tjohncollege.com/happiness">Happiness</a></li>
							<li><a href="http://alumni.tjohncollege.com/">Alumni</a></li>
                            <li <?php if($list['page']=='news' || $list['page']=='press' || $list['page']=='academic_calendar' ){?> class="active dropdown" <?php } else {?> class="dropdown" <?php }  ?>><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Newsroom <span class="caret"></span></a>
                                <ul class="dropdown-menu" >
                                    <li <?php if($list['page']=='news' ){?> class="active" <?php } ?>><a href="<?php echo NEW_URL; ?>news">News</a></li>
                                    <li <?php if($list['page']=='press'){?> class="active" <?php } ?>><a href="<?php echo NEW_URL; ?>presscoverage">Press Coverage</a></li>								<li <?php if($list['page']=='academic_calendar'){?> class="active" <?php } ?>><a href="<?php echo NEW_URL; ?>academic-calendar">Academic Calendar</a></li>
                                </ul>
                            </li>
                            <li <?php if($list['page']=='events'){?> class="active dropdown" <?php } else{ ?>class="dropdown" <?php } ?>><a href="#">Events<span class="caret"></span></a>
                             <ul class="dropdown-menu" >
                             	<?php $i=0;
                                foreach($list['college'] as $collegeval)
                                { $i++;
								?>
                                 	<li <?php if($list['page']==$collegeval['college_name'] ){?> class="active" <?php } ?>><a href="<?php echo NEW_URL; ?>event/college/<?php echo str_replace(' ','_',$collegeval['college_name']);?>"><?php echo $collegeval['college_name']?>
									<?php if($i<3) { ?> <span class="caret"></span><?php } ?></a>
									<?php if($i<3) { ?>
                                    <ul class="dropdown-menu">
                                                        <?php
                                                        foreach($list['department'] as $deptval)
                                                        {
                                                            if($deptval['department_collegeid']== $collegeval['college_id'] &&$deptval['show_in_event']==1)
                                                            {
                                                             ?>
                                                           		 <li <?php if($list['page']==$deptval['department_name']){?> class="active" <?php } ?>><a href="<?php echo NEW_URL; ?>event/department/<?php echo str_replace(' ','_',$deptval['department_name']);?>"><?php echo $deptval['department_name']?> </a></li>
                                                            <?php
                                                            }
                                                        }
                                                            ?>
                                                            
                                                        </ul>
														<?php } ?>
                                    </li>
                                    <?php
								}
														?>
                                </ul>
                            </li>
                             <li <?php if($list['page']=='gallery'){?> class="active" <?php } ?>><a href="<?php echo NEW_URL; ?>gallery">Gallery</a></li>
                            
							<li <?php if($list['page']=='contact'){?> class="active" <?php } ?>><a href="<?php echo NEW_URL; ?>contact_us">Contact Us</a></li>
							
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>