<div class="container margin-top-10">
  <div class="row">
        <div class="breadcrumb">
            <?php echo $list['breadcrumb']; ?>  
        </div>
    </div>
	<div class="row">
		<div class="col-xs-12 col-md-8 col-lg-9">
<!--         <ul class="calendar-list">
<li>T. John College UG<a href="https://www.tjohncollege.com/calendar_event/TJC_UG.pdf" target="_blank" >View</a></li>
<li>T. John College PG<a href="https://www.tjohncollege.com/calendar_event/TJC_PG.pdf" target="_blank">View</a></li>
<li>T. John Institute of Management & Science UG <a href="https://www.tjohncollege.com/calendar_event/TIMS_UG.pdf" target="_blank">View</a></li>
<li>T. John Institute of Management & Science PG <a href="https://www.tjohncollege.com/calendar_event/TIMS_PG.pdf" target="_blank">View</a></li>
<li>T. John Institute of Technology UG<a href="https://www.tjohncollege.com/calendar_event/TJIT_UG.pdf" target="_blank">View</a></li>
<li>T. John College of Pharmacy<a href="https://www.tjohncollege.com/calendar_event/TJCP.pdf" target="_blank">View</a></li>
</ul> -->        
		<ul class="calendar-list">
			<?php foreach($list['calendar'] as $data) { ?>
			<li><?php echo $data['homeflashmessage_content']; ?><a href="<?php echo $data['homeflashmessage_url']; ?>" target="_blank" >View</a></li>
			<?php } ?>
		</ul>
		</div>
        <div class="col-xs-12 col-md-4 col-lg-3">
			<?php include 'sidebar.php'; ?>
		</div>
		
	</div>
</div>