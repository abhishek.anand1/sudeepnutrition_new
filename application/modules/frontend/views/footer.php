<style> 
.hi-lte{animation-name: color_change;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-direction: alternate;}
    
@-webkit-keyframes color_change {
    from { color: red; }
    to { color: #4ab0ff; }
}
@-moz-keyframes color_change {
       from { color: red; }
    to { color: #4ab0ff; }
}
@-ms-keyframes color_change {
       from { color: red; }
    to { color: #4ab0ff; }
}
@-o-keyframes color_change {
       from { color: red; }
    to { color: #4ab0ff; }
}
@keyframes color_change {
    from { color: red; }
    to { color: #4ab0ff; }
}
</style>
<footer>
    <div class="footer-menu">
    	<div class="container">
    	<div class="row">
        	<div class="col-xs-12 col-md-4 col-lg-4 rp-mb-30">
            	<h3>Students Links</h3>
                <ul class="student-link">
                <?php if(!empty($list['link'][0]['link_1'])):?>
                	<li><a href="<?php echo @$list['link'][0]['link_1']; ?>" target="_blank"><?php echo @$list['link'][0]['text_1']; ?></a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_2'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_2'];?>" target="_blank"><?php echo @$list['link'][0]['text_2']; ?></a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_3'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_3']; ?>" target="_blank"><?php echo @$list['link'][0]['text_3']; ?></a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_4'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_4']; ?>" target="_blank"><?php echo @$list['link'][0]['text_4']; ?></a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_5'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_5'];?>" target="_blank"><?php echo @$list['link'][0]['text_5']; ?></a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_6'])):?>
                    
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_7'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_7'];?>" target="_blank"><?php echo @$list['link'][0]['text_7']; ?></a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_8'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_8'];?>" target="_blank"><?php echo @$list['link'][0]['text_8']; ?> </a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_9'])):?>
                     <li><a href="<?php echo @$list['link'][0]['link_9'];?>" target="_blank"><?php echo @$list['link'][0]['text_9']; ?></a></li>
                     <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_10'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_10'];?>" target="_blank"><?php echo @$list['link'][0]['text_10']; ?> </a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_11'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_11'];?>" target="_blank"><?php echo @$list['link'][0]['text_11']; ?></a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_12'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_12'];?>" target="_blank"><?php echo @$list['link'][0]['text_12']; ?> </a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_13'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_13'];?>" target="_blank"><?php echo @$list['link'][0]['text_13']; ?></a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_14'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_14'];?>" target="_blank"><?php echo @$list['link'][0]['text_14']; ?> </a></li>
                    <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_6'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_6'];?>" target="_blank"><?php echo @$list['link'][0]['text_6']; ?> </a></li>
                    <?php endif; ?>
                    <li class="alumini-li"><a href="<?php echo @$list['link'][0]['link_38'];?> " target="_blank" title="T John Alumini"><img src="<?php echo base_url()?>assets/images/alumininew-logo.png" width="42" height="42" alt="T John Alumini"></a></li>
                </ul>
                
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4 rp-mb-30">
            	<h3>Important Links</h3>
                <ul>
                 <?php if(!empty($list['link'][0]['link_15'])):?>
               		<li><a href="<?php echo @$list['link'][0]['link_15']; ?>" ><?php echo @$list['link'][0]['text_15']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_16'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_16'];?>" target="_blank"><?php echo @$list['link'][0]['text_16']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_17'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_17']; ?>" target="_blank"><?php echo @$list['link'][0]['text_17']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_18'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_18']; ?>" target="_blank"><?php echo @$list['link'][0]['text_18']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_19'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_19'];?>" ><?php echo @$list['link'][0]['text_19']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_20'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_20'];?>"><?php echo @$list['link'][0]['text_20']; ?> </a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_21'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_21']; ?>" target="_blank"><?php echo @$list['link'][0]['text_21']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_22'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_22'];?>"><?php echo @$list['link'][0]['text_22']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_23'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_23']; ?>" target="_blank"><?php echo @$list['link'][0]['text_23']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_24'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_24']; ?>"><?php echo @$list['link'][0]['text_24']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_25'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_25'];?>"><?php echo @$list['link'][0]['text_25']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_26'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_26'];?>" target="_blank"><?php echo @$list['link'][0]['text_26']; ?> </a></li>
                     <?php endif; ?>
                </ul>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4 rp-mb-30">
            	<h3>Social Media</h3>
                <ul class="social">
                	<?php if(!empty($list['link'][0]['link_29'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_29'];?>" target="_blank" title="Blog"><i class="fa fa-rss-square" aria-hidden="true"></i></i></a></li>
                <?php endif; ?>
                <?php if(!empty($list['link'][0]['link_33'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_33'];?>" target="_blank" title="LinkedIn"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($list['link'][0]['link_34'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_34'];?>" target="_blank" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($list['link'][0]['link_32'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_32'];?>" target="_blank" title="Youtube"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($list['link'][0]['link_30'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_30'];?>" target="_blank" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($list['link'][0]['link_35'])):?>
                	<li><a href="<?php echo @$list['link'][0]['link_35'];?>" target="_blank"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($list['link'][0]['link_31'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_31'];?>" target="_blank"><i class="fa fa-picture-o" aria-hidden="true"></i></i></a></li>
                    <?php endif; ?>
                    <li><a href="<?php echo @$list['link'][0]['link_36'];?>" target="_blank" title="Awards"><i class="fa fa-trophy" aria-hidden="true"></i></i></a></li>
                    <li><a href="<?php echo @$list['link'][0]['link_37'];?> " target="_blank" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                     <!-- <li><a href="<?php echo @$list['link'][0]['link_38'];?> " target="_blank" title="T John Alumini"><img src="<?php echo base_url()?>assets/images/alumni1.png" width="32" height="32" alt="T John Alumini"></a></li> -->
                </ul>
                <!--<h3><a href="<?php //echo @$list['link'][0]['link_6'];?>" target="_blank" class="hi-lte"><?php //echo @$list['link'][0]['text_6']; ?> </a></h3>
                <ul class="social"> <li><a href="<?php echo @$list['link'][0]['link_6'];?>" target="_blank"><i <i class="fa fa-trophy" aria-hidden="true"></i></i></a></li></ul>
                <ul>-->
                   <?php if(!empty($list['link'][0]['link_36'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_36']; ?>"><?php echo @$list['link'][0]['text_24']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_37'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_37'];?>"><?php echo @$list['link'][0]['text_25']; ?></a></li>
                     <?php endif; ?>
                     <?php if(!empty($list['link'][0]['link_38'])):?>
                    <li><a href="<?php echo @$list['link'][0]['link_38'];?>" target="_blank"><?php echo @$list['link'][0]['text_26']; ?> </a></li>
                     <?php endif; ?>
                </ul>
                <!--<div class="address">
                	<h4>Address</h4>
                   <?php echo @$list['contactadress'][0]['contactaddress_address'];?>
                   <?php /*?> <div class="phone">+47823789123</div><?php */?>
                    
                </div> -->
            </div>
        </div>
    </div>
    </div>
	<div class="container copyright">
    	<div class="row">
        	<div class="col-md-12">
            	<div class="row">
                	<div class="col-xs-12 col-md-6 col-lg-6 rp-mb-10"><p>Copyright © 2017 T. John Group of Institutions. All Rights Reserved.</p></div>
                    <div class="col-xs-12 col-md-2 col-lg-2 rp-mb-10"><p> Our Visitors as of now:</p></div> 
                   <div class="col-xs-12 col-md-3 col-lg-3 rp-mb-10"> <img src='https://www.hit-counts.com/counter.php?t=15&digits=7&ic=346287&cid=1153411' border='0'alt='Hit Counter'>
                    </div>
                	<?php /*?><div class="col-xs-12 col-md-6 col-lg-6 rp-text-right">Dgital marketing solution by <a href="http://codelattice.com/" target="_blank">Codelattice</a></div><?php */?>
                   
                </div>
            	
            
            </div>
            
        </div>
    </div>
    <a class="scrollup" href="#top" style="display: block;">
<i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
</a>
</footer>
<script src="<?php echo base_url()?>assets/js/jquery-1.12.0.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.smartmenus.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.smartmenus.bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.sky.carousel-1.0.2.min.js"></script>
<?php /*?><script src="<?php echo base_url()?>assets/js/dcalendar.picker.js"></script><?php */?>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>assets/js/custom.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/underscore-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jstz.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/app.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="../js/excanvas/excanvas.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/spinners.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/lightview.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.responsiveTabs.js"></script>
