<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

<title>Plupload - jQuery UI Widget</title>

<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/upload_js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url()?>assets/admin_css/theme-default.css"/>



<!-- debug 
<script type="text/javascript" src="../../js/moxie.js"></script>
<script type="text/javascript" src="../../js/plupload.dev.js"></script>
<script type="text/javascript" src="../../js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
-->

</head>
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?php echo base_url()?>admin/dashboard">Resolution Talent </a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">John Doe</div>
                                <div class="profile-data-title">Web Developer/Designer</div>
                            </div>
                            <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
                        </div>                                                                        
                    </li>
                  
                    <li class="active">
                        <a href="<?php echo base_url()?>admin/dashboard"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Country</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/country/add"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/country"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Ethnicity</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/ethnicity/add"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/ethnicity"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>
                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Accent</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/accent/add"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/accent"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Location Types</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/locations/addtype"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/locations/loadtype"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>                
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Actors</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/actors/add"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/actors"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>
                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Model</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/models/add"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/models"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>
                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Voices</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/voices/add"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/voices"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>
                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Childrens</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/childrens/add"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/childrens"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>
                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Animals</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/animals/add"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/animals"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>
                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Locations</span></a>
                        <ul>
                            <li><a href="<?php echo base_url()?>admin/locations/add"><span class="fa fa-image"></span> Add</a></li>
                            <li><a href="<?php echo base_url()?>admin/locations"><span class="fa fa-user"></span> View</a></li>
                        </ul>
                    </li>
                                     
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                   <!--  <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>  --> 
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- MESSAGES -->
                    <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-comments"></span></a>
                        <div class="informer informer-danger">4</div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>                                
                                <div class="pull-right">
                                    <span class="label label-danger">4 new</span>
                                </div>
                            </div>
                            <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-online"></div>
                                    <img src="assets/images/users/user2.jpg" class="pull-left" alt="John Doe"/>
                                    <span class="contacts-title">John Doe</span>
                                    <p>Praesent placerat tellus id augue condimentum</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="assets/images/users/user.jpg" class="pull-left" alt="Dmitry Ivaniuk"/>
                                    <span class="contacts-title">Dmitry Ivaniuk</span>
                                    <p>Donec risus sapien, sagittis et magna quis</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="assets/images/users/user3.jpg" class="pull-left" alt="Nadia Ali"/>
                                    <span class="contacts-title">Nadia Ali</span>
                                    <p>Mauris vel eros ut nunc rhoncus cursus sed</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-offline"></div>
                                    <img src="assets/images/users/user6.jpg" class="pull-left" alt="Darth Vader"/>
                                    <span class="contacts-title">Darth Vader</span>
                                    <p>I want my money back!</p>
                                </a>
                            </div>     
                            <div class="panel-footer text-center">
                                <a href="pages-messages.html">Show all messages</a>
                            </div>                            
                        </div>                        
                    </li>
                    <!-- END MESSAGES -->
                    <!-- TASKS -->
                    <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-tasks"></span></a>
                        <div class="informer informer-warning">3</div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-tasks"></span> Tasks</h3>                                
                                <div class="pull-right">
                                    <span class="label label-warning">3 active</span>
                                </div>
                            </div>
                            <div class="panel-body list-group scroll" style="height: 200px;">                                
                                <a class="list-group-item" href="#">
                                    <strong>Phasellus augue arcu, elementum</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 25 Sep 2014 / 50%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Aenean ac cursus</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">80%</div>
                                    </div>
                                    <small class="text-muted">Dmitry Ivaniuk, 24 Sep 2014 / 80%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Lorem ipsum dolor</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">95%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 23 Sep 2014 / 95%</small>
                                </a>
                                <a class="list-group-item" href="#">
                                    <strong>Cras suscipit ac quam at tincidunt.</strong>
                                    <div class="progress progress-small">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                    </div>
                                    <small class="text-muted">John Doe, 21 Sep 2014 /</small><small class="text-success"> Done</small>
                                </a>                                
                            </div>     
                            <div class="panel-footer text-center">
                                <a href="pages-tasks.html">Show all tasks</a>
                            </div>                            
                        </div>                        
                    </li>
                    <!-- END TASKS -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->   
            
        <!-- END PAGE CONTAINER -->
                        <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Actors</a></li>
                    <li><a href="#">Register</a></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE CONTENT WRAPPER -->
				<div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START TABS -->                                
                            <div class="panel panel-default tabs">                            
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">General Information</a></li>
                                    <li><a href="#tab-second" role="tab" data-toggle="tab">Portfolio</a></li>
                                    <li><a href="#tab-third" role="tab" data-toggle="tab">Video</a></li>
                                    <li><a href="#tab-fourth" role="tab" data-toggle="tab">Fourth</a></li>
                                </ul>                            
                                <div class="panel-body tab-content">
                                <div class="tab-pane active" id="tab-first">
                                <form id="jvalidate" class="form-horizontal" action="<?php echo BASE_URL?>admin/actors/insert" method="post">
                            	<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Actors</strong> Registration Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                       
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">First Name</label>
                                        <div class="col-md-6">                                             
                                                <input type="text" class="form-control" name="fname" id="fname"/>                                
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Last Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                                <input type="text" class="form-control" name="lname"/>                                       
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Gender</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="col-md-4">                                    
                                            <label class="check"><input type="radio" name="gender" class="iradio" name="iradio" value="male" checked="checked"/> Male</label>
                                        </div>
                                        <div class="col-md-4">                                    
                                            <label class="check"><input type="radio"  name="gender" class="iradio" name="iradio" value="female" /> Female</label>
                                        </div>
                                        </div>
                                     </div>
                                     
                           			 <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Age</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="col-md-2">                                    
                                            <label class="check"><input type="radio" id="age"  name="age" class="iradio" name="iradio" value="18-25" checked="checked"/> 18-25</label>
                                        	</div>
                                        	<div class="col-md-2">                                    
                                            <label class="check"><input type="radio" id="age" name="age" class="iradio" name="iradio" value="26-35" /> 26-35</label>
                                        	</div>
                                            <div class="col-md-2">                                    
                                            <label class="check"><input type="radio" id="age" name="age" class="iradio" name="iradio" value="36-45" /> 36-45</label>
                                        	</div>
                                            <div class="col-md-2">                                    
                                            <label class="check"><input type="radio" id="age" name="age" class="iradio" name="iradio" value="46-60" /> 46-60</label>
                                        	</div>
                                            <div class="col-md-2">                                    
                                            <label class="check"><input type="radio" id="age" name="age" class="iradio" name="iradio" value="61-75" /> 61-75</label>
                                        	</div>
                                        </div>
                                     </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Country Of Birth</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control select" name="cbirth" id="cbirth">
                                            	<option value="">---select---</option>
                                             	<?php
												foreach($list['country'] as $countryval)
												{
													?>
														<option value="<?php echo $countryval['country_id']?>"><?php echo $countryval['country_name']?></option>
												<?php
												}
												?>
                                            </select>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Ethnicity</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control select" name="ethnicity" id="ethnicity">
                                            	<option value="">---select---</option>
                                                <?php
												foreach($list['ethnicity'] as $ethnicityval)
												{
													?>
														<option value="<?php echo $ethnicityval['ethnicity_id']?>"><?php echo $ethnicityval['ethnicity_name']?></option>
												<?php
												}
												?>
                                            </select>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Key word/Skills</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <textarea class="form-control" rows="5" name="skills"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           </div>
                           <div class="tab-pane" id="tab-second">       
                            	<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Actors</strong> Registration Form</h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <p></p>
                                </div>
                                <div class="panel-body">                                                                        
                                       
                                 <form id="form" method="post" action="../dump.php">
                                        <div id="uploader">
                                            <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                                        </div>
                                        <br />
                                        <input type="submit" value="Submit" />
                                   </form>
                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-default">Clear Form</button>                                    
                                    <input type="submit"class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                            </form>
                                    </div>
                                    <div class="tab-pane" id="tab-third">
                                        <p>Vestibulum cursus augue sed leo tempor, at aliquam orci dictum. Sed mattis metus id velit aliquet, et interdum nulla porta. Etiam euismod pellentesque purus, in fermentum eros venenatis ut. Praesent vitae nibh ac augue gravida lacinia non a ipsum. Aenean vestibulum eu turpis eu posuere. Sed eget lacus lacinia, mollis urna et, interdum dui. Donec sed diam ut metus imperdiet malesuada. Maecenas tincidunt ultricies ipsum, lobortis pretium dolor sodales ut. Donec nec fringilla nulla. In mattis sapien lorem, nec tincidunt elit scelerisque tempus.</p>
                                    </div>
                                    <div class="tab-pane" id="tab-fourth">
                                        <p>In mattis sapien lorem, nec tincidunt elit scelerisque tempus. Quisque nisl nisl, venenatis eget dignissim et, adipiscing eu tellus. Sed nulla massa, luctus id orci sed, elementum consequat est. Proin dictum odio quis diam gravida facilisis. Sed pharetra dolor a tempor tristique. Sed semper sed urna ac dignissim. Aenean fermentum leo at posuere mattis. Etiam vitae quam in magna viverra dictum. Curabitur feugiat ligula in dui luctus, sed aliquet neque posuere.</p>
                                    </div>
                                </div>
                            </div>                                                   
                            <!-- END TABS -->                        
                        </div>

                    </div>               

                    
                    
                </div> 
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        </div>
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/scrolltotop/scrolltopcontrol.js"></script>
        
       
       
                     
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
                        
        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/settings.js"></script>
        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/actions.js"></script>
        
        <!--<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/demo_dashboard.js"></script>-->
        <!-- END TEMPLATE -->
        
         <!-- JS for form lements  -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
         <!-- END forms  -->
         
          <!-- Data Data table   -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/tableExport.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jquery.base64.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/html2canvas.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jspdf/jspdf.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/admin_js/plugins/tableexport/jspdf/libs/base64.js"></script> 
         <!-- END Data table  -->
          <!-- validation   -->
               
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/validationengine/jquery.validationEngine.js'></script>        
        <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/jquery-validation/jquery.validate.js'></script> 
          <script type='text/javascript' src='<?php echo base_url()?>assets/admin_js/plugins/maskedinput/jquery.maskedinput.min.js'></script>
         <!-- END  -->
         

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<!-- production -->
<script type="text/javascript" src="<?php echo base_url()?>assets/upload_js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/upload_js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
<script type="text/javascript">
// Initialize the widget when the DOM is ready
$(function() {
 $("#uploader").plupload({
            multipart_params: {},
            runtimes: 'html5,flash,silverlight,html4',
            url: 'http://your/ci/installation/folder/uploader/do_upload',
            ax_file_count: 20,
            max_file_size: '10mb',
            chunks: true,
            chunk_size: '3mb',
            resize: {
            width: 640,
            height: 480,
            quality: 100,
            crop: false
        },
		
		filters : {
			// Maximum file size
			max_file_size : '1000mb',
			// Specify what files to browse for
			mime_types: [
				{title : "Image files", extensions : "jpg,gif,png"},
				{title : "Zip files", extensions : "zip"}
			]
		},

		// Rename files by clicking on their titles
		rename: true,
		
		// Sort files
		sortable: true,

		// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
		dragdrop: true,

		// Views to activate
		views: {
			list: true,
			thumbs: true, // Show thumbs
			active: 'thumbs'
		},

		// Flash settings
		flash_swf_url : '<?php echo base_url()?>assets/upload_js/Moxie.swf',

		// Silverlight settings
		silverlight_xap_url : '<?php echo base_url()?>assets/upload_js/Moxie.xap'
	});


	// Handle the case when form was submitted before uploading has finished
	$('#form').submit(function(e) {
		// Files in queue upload them first
		if ($('#uploader').plupload('getFiles').length > 0) {

			// When all files are uploaded submit form
			$('#uploader').on('complete', function() {
				$('#form')[0].submit();
			});

			$('#uploader').plupload('start');
		} else {
			alert("You must have at least one file in the queue.");
		}
		return false; // Keep the form from submitting
	});
});
</script>
<script type="text/javascript">
            var validate = $("#jvalidate").validate({
				
                ignore: [],
                rules: {                                            
                        fname: {
                                required: true,                                
                        },
                        lname: {
                                required: true,
                        },
                        
                        ethnicity: {
                                required: true,
                        },
                        cbirth: {
                                required: true,
                        },
						age: {
                                required: true,
                        },
						name: {
                                required: true,
                        },
						type: {
                                required: true,
                        }
						
                     
                    }   
					                                   
                });                                    

        </script>   
</body>
</html>
