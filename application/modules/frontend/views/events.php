<div class="container margin-top-10">
<div class="row">
        <div class="breadcrumb">
            <?php echo $list['breadcrumb']; ?>  
        </div>
    </div>
	<div class="row">
		<div class="col-xs-12 col-md-8 col-lg-9">
			<?php /*?><div class="inner-banner">
				<img src="<?php echo base_url()?>assets/images/about-banner.jpg" width="750" height="350" alt="T John College">
			</div><?php */?>
			<div class="main-content" style="padding-bottom:0;">
            
             <?php 
			
			if(count($list['underdepartment'])>0)
			{
				?>
                    <div class="thirdmenu">   
                         <div class="head">
                         	<?php /*?><h6><?php echo $list['page'];?></h6><?php */?>
                            <h6>Departments</h6>
                         </div>
                          <ul>
                          <?php foreach($list['underdepartment'] as $underdepartment)
						  {
							  ?>
                               <li><a title="FLEXCUBE" href="<?php echo base_url()?>event?department=<?php echo $underdepartment['department_id']?>"><?php echo $underdepartment['department_name'];?></a></li>
                            <?php
						  }
						  ?>
                           </ul>
                  	</div>
                    <?php
			}
			?>
				<h3></h3>
				 <?php
				 if(count($list['events'])>0)
				 {
				foreach($list['events'] as $eventsval)
		{
			?>
					<h4><a href="<?php echo base_url()?>event/details/<?php echo $eventsval['events_id'];?>"><?php echo $eventsval['events_title'];?>: <?php if($eventsval['events_date']!="1970-01-01") {echo date('d-m-Y',strtotime($eventsval['events_date']));}?></a></h4>
					
					<p><?php echo $eventsval['events_shortcontent'];?></p>
<hr />
<?php
		}
		
        }
        else
        {
			echo "No events found";
		}
			?>

			</div>
            <div class="pagination-container">
                <nav class="pagination">
                    <ul>
                        <?php foreach ($list['links'] as $link) {
							echo "<li>". $link."</li>";
							} ?>
                    </ul>
                </nav>
			</div>
		</div>
		<div class="col-xs-12 col-md-4 col-lg-3">
			<?php include 'sidebar.php'; ?>
		</div>
	</div>
</div>