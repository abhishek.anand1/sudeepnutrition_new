<style>
.ln-column {
    text-align: center;
}
/*.banner-text2 {
    bottom: 2px;
    color: #fff;
    font-size: 40px;
    font-weight: 600;
    left: 0;
    padding: 5px 25px;
    position: absolute;
    right: 0;
    background: rgba(0, 0, 0, 0.38) none repeat scroll 0 0;
    font-size: 29px;
        text-align: center;
}*/

.banner-text2 {

    bottom: 2px;
    color: #fff;
    font-size: 40px;
    font-weight: 600;
    left: 60px;
    padding: 5px 25px;
    position: absolute;
    font-size: 29px;
    text-align: center;
    top: 0;
    display: flex;
    align-items: center;

}
.banner-text-inner {

    text-align: left;

}
.banner-text-inner h2 {

    width: 100%;
    display: block;
    font-weight: 600;
    font-size: 71px;
    line-height: 1;
    margin-bottom: 0;
    color: #CB3C35;

}

.banner-text-inner h3 {

    font-weight: 700;
    color: #000;
    font-size: 32px;
    margin-top: 33px;

}

.banner-text2 a {

    color: #FFFFFF;
    width: 100%;
    text-align: center;
    display: block;
    font-weight: 400;
    font-size: 26px;
    background: #16569a;
    padding: 20px;

}

.banner-text-inner h4 {

    font-size: 29px;
    color: #333;

}
@media (max-width: 480px) {
.banner-text2{font-size: 18px;}

.banner img {
    height: auto;
    width: 100%;
}
.banner-text-inner h2 {
   font-size: 18px;
}

.banner-text-inner h3 {
    font-size: 16px;
    margin: 5px 0 0;
}
.banner-text-inner h4 {
    font-size: 14px;
}

.banner-text2 a {
    font-size: 12px;
    padding: 7px;
    margin: 8px 0;
}

}

@media (max-width: 760px) {
.banner-text2{font-size: 22px;}

.banner img {
    height: auto;
    width: 100%;
}
.banner-text-inner h2 {
   font-size: 24px;
}

.banner-text-inner h3 {
    font-size: 18px;
    margin: 7px 0 0;
}
.banner-text-inner h4 {
    font-size: 16px;
}

.banner-text2 a {
    font-size: 14px;
    padding: 9px;
    margin: 10px 0;
}

}

.banner-text2 a {
     color: #FFFFFF;
}
</style>
<div class="banner owl-carousel owl-theme">
 <?php
    	foreach($list['slider'] as $val)
		{
			?>
	<div class="slide-item">
        <img src="<?php echo base_url()?>assets/homeslider/<?php echo $val['homeslider_image']?>">
        <div class="banner-text2">
        <div class="banner-text-inner" >
            <?php if(strlen($val['homeslider_page']) > 3) { ?>
            <h2>Admisson for</h2>
            <h2> B.Tech 2019</h2>
            <h3>Scholarship Up-to 1 Lakh.</h3>
            <a  href="<?php echo $val['homeslider_page']; ?>"> <?php echo $val['homeslider_title']?></a>
            <h4>Last Date: 1st Aug 2019</h4>
            <?php } ?>
        </div>
        </div>
    </div>
    <?php
		}
		?>
    
</div>


<div class="container ln-container" style="margin-top: 0px;">
	<div class="row">
    	<div class="col-xs-12 col-md-4 col-lg-4">
        <a href="<?php echo @$list['threebox'][0]['homethreebox_link1']?>" target="_blank">
        	<div class="ln-column">
        	<h3><?php echo @$list['threebox'][0]['homethreebox_title1'];?></h3>
            <div class="row">
            	<div class="col-xs-8 col-sm-9 col-lg-8">
                <?php echo @$list['threebox'][0]['homethreebox_content1'];?>
                </div>
                <div class="col-xs-4 col-sm-3 col-lg-4 text-right">
                	<img src="<?php echo base_url()?>assets/gallery/<?php echo @$list['threebox'][0]['homethreebox_image1']?>">
                </div>
            </div>
            </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-4  col-lg-4">
         <a href="<?php echo @$list['threebox'][0]['homethreebox_link2']?>" target="_blank">
        	<div class="ln-column">
        	<h3><?php echo @$list['threebox'][0]['homethreebox_title2'];?></h3>
            <div class="row">
            	<div class="col-xs-8 col-sm-9 col-lg-8">
                <?php echo @$list['threebox'][0]['homethreebox_content2'];?> 
                </div>
                <div class="col-xs-4 col-sm-3 col-lg-4 text-right">
                	<img src="<?php echo base_url()?>assets/gallery/<?php echo @$list['threebox'][0]['homethreebox_image2']?>">
                </div>
            </div>
            </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-4  col-lg-4">
         <a href="<?php echo @$list['threebox'][0]['homethreebox_link3']?>" target="_blank">
        	<div class="ln-column">
        	<h3><?php echo @$list['threebox'][0]['homethreebox_title3'];?></h3>
            <div class="row">
            	<div class="col-xs-8 col-sm-9 col-lg-8">
               <?php echo @$list['threebox'][0]['homethreebox_content3'];?>
                </div>
                <div class="col-xs-4 col-sm-3 col-lg-4 text-right">
                <img src="<?php echo base_url()?>assets/gallery/<?php echo @$list['threebox'][0]['homethreebox_image3']?>">
                </div>
            </div>
            </div>
            </a>
        </div>
    </div>
	
</div>


<div class="container">
	<div class="notification">
     <?php
		foreach($list['news'] as $val)
		{
			?>
    	<li><a href="<?php echo base_url()?>news/details/<?php echo $val['news_id'];?>" ><?php echo $val['news_shortcontent']?></a></li>
       <?php
		}
		?>
    </div>
</div>


<div class="placement-wrapper">
<div class="container">
<h2 class="center-head">Placements of this year</h2>
<div class="sixteen columns">
			<div class="project">
				<div class="sky-carousel">
					<div class="sky-carousel-wrapper">
						<ul class="sky-carousel-container">
                          <?php
		foreach($list['placements'] as $val)
		{
			?>
							<li>
								<img src="<?php echo base_url()?>assets/placementsprofile/<?php echo $val['placements_candidateimage']?>" alt="" />
								<div class="sc-content">
									<h2><?php echo $val['placements_candidatename']?><span><?php echo $val['placements_description']?></span><span><?php echo $val['placements_company']?></span></h2>
                                    
								</div>
							</li>
						  <?php
		}
		?>	
						</ul>
					</div>
				</div>
			</div>
		</div>
</div>
</div>






<div class="container margin-bottom-50">
<h2 class="center-head">About T.John</h2>
   <?php
		foreach($list['about'] as $val)
		{
			?>
<p><?php echo $val['homeabout_content']?></p>
 <?php
		}
		?>	
</div>

<div class="event-container">
	<div class="container">
    	<div class="row">
        	<div class="col-xs-12 col-md-4 col-lg-4 rp-mb-30">
            	<h3>Event Calendar</h3>
                 <div class="btn-group cal-btn">
                <button class="btn btn-primary" data-calendar-view="year">Year</button>
                <button class="btn btn-primary active" data-calendar-view="month">Month</button>
				<button class="btn btn-primary" data-calendar-nav="prev"><</button>
				<button class="btn btn-primary" data-calendar-nav="next">></button>
			</div>
            <div class="page-header">
            <h3></h3>
            </div>
                <div id="calendar" class="calendar ">
                
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4 rp-mb-30">
            	<!--<h3>Academic Calendar</h3>-->
               <!--<div class="newsslider">
     <?php
	 $fcount=count($list['flashmessage']);
		for($i=0;$i<$fcount;$i+=2)
		{
			?>
    		<li>
        		<div class="newsslider-first">
					<?php echo $list['flashmessage'][$i]['homeflashmessage_content'];?>
        			<fieldset class="form-group text-right">
						<a href="<?php echo  $list['flashmessage'][$i]['homeflashmessage_url']?>" target="_blank"><input type="submit" class="btn dwnbtn" value="Download" /></a>
					</fieldset>
                </div>
                <?php if($i+1<$fcount)
				{
				?>
                 <div>
					<?php
                     echo  $list['flashmessage'][$i+1]['homeflashmessage_content'];?>
                      <fieldset class="form-group text-right">
                         <a href="<?php echo  $list['flashmessage'][$i+1]['homeflashmessage_url']?>" target="_blank"><input type="submit" class="btn dwnbtn" value="Download" /></a>
                       </fieldset>
                   </div>
                 <?php
                 }
                 ?>
        	</li>
       <?php
		}
		?>
    </div>-->
            </div>
            <style>
            .rp-mb-30 a{
              color: #fff;
              text-decoration: none;
              }

            </style>
            <div class="col-xs-12 col-md-4 col-lg-4 rp-mb-30">
            	<!--<a href="<?php echo @$list['link'][0]['link_14'];?>" target="_blank"><h3>Click Here</h3>-->
		        <a href="<?php echo $list['hostel'][0]['link'];?>" target="_blank"><h3>Campus Hostel</h3>
                <p><img src="<?php echo base_url()?>assets/images/<?php echo $list['hostel'][0]['image'];?>"></p>
                <p><?php echo $list['hostel'][0]['title'];?></p></a>
            </div>
        </div>
    </div>
</div>

<div class="container white-container">
 <?php
		foreach($list['about'] as $val)
		{
			?>
           <!-- <h1 class="text-center">"<?php // echo $val['homeabout_oneline']?>"</h1>-->

 <?php
		}
		?>	
	
</div>
