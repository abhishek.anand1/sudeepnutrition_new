<div class="container margin-top-10">
<div class="row">
        <div class="breadcrumb">
            <?php echo $list['breadcrumb']; ?>  
        </div>
    </div>
	<div class="row">
		<div class="col-xs-12 col-md-8 col-lg-9">
			
           
			<div class="main-content">
           
                	<div>
                        <h3> <h2>Staff Links</h2></h3>
                        
<table width="100%" cellpadding="10" class="stafflink-table">
	<tbody>
		<tr>
			<th>
				<h5>Webmails:</h5>
			</th>
			<th>
				<h5>Rockefeller Habits:</h5>
			</th>
		</tr>
		<tr>
			<td>
				<p><a href="http://webmail.tjohngroup.com/" target="_blank">TJC</a> | <a href="http://webmail.tjohnit.com/" target="_blank">TJIT</a> | <a href="http://webmail.tjohngroup.com/" target="_blank">TJCP</a> | <a href="http://webmail.tjohngroup.com/" target="_blank">TJCN</a> | <a href="http://webmail.tjohngroup.com/" target="_blank">TIMS</a> | <a href="http://webmail.tjohngroup.com/" target="_blank">TJGI</a>
				</p>
			</td>
			<td>
				<p><a href="http://www.tjohncollege.com/tjcrh/" target="_blank">TJC</a> | <a href="http://www.tjohncollege.com/tjitrh/" target="_blank">TJIT</a> | <a href="http://www.tjohncollege.com/tjcprh/" target="_blank">TJCP</a> | <a href="http://www.tjohncollege.com/tjcnrh/" target="_blank">TJCN</a> | <a href="http://www.tjohncollege.com/timsrh/" target="_blank">TIMS</a>
					<a href="http://www.tjohncollege.com/tjgirh/" target="_blank">
						<br />
					</a>
				</p>
<em><a href="http://tjohncollege.com/kaizen" target="_blank">Kaizen Dashboard</a></em>
			</td>
		</tr>
		<tr>
			<th align="left" scope="row"> <a href="http://tjohncollege.com/index.php/component/content/article/81-staff-links/116-tjgi-mist-login-portal">MIST Login Portal</a>
			</th>
			<th align="left" scope="row"> <a href="http://www.tjohncollege.com/edataportal" target="_blank">e-Data portal</a>
			</th>
		</tr>
		<tr>
			<th align="left" scope="col">
				<p>Oracle Peoplesoft Links
					<br />(Outside Campus Only)</p>
				<p><a href="http://122.181.173.92:8200/HRPRD92/signon.html" target="_blank">1.HR Applications.</a>
				</p>
				<p> <a href="http://campus.tjohncollege.com/" target="_blank">2.Campus Solutions Applications.</a>
				</p>
				<p> <a href="http://122.181.173.92:8600/FSPRD92/signon.html" target="_blank">3.Finance &amp; Purchase Applications.</a>
				</p>
			</th>
			<th align="left" scope="col">Oracle Peoplesoft Links<br />(Within Campus Only)
				<p><em><a href="http://tjohncollege.com/docs/peoplesoftmanual.pdf" target="_blank">Peoplesoft Manuals</a></em></p>
				<p><a href="http://10.41.0.10:8200/HRPRD92/signon.html" target="_blank">1.HR Applications.</a></p>
					
					<p><a href="http://campus.tjohncollege.com/" target="_blank">2.Campus Solutions Applications.</a></p>
					<p><a href="http://10.41.0.10:8600/FSPRD92/signon.html" target="_blank">3.Finance &amp; Purchase Applications.</a></p>
			</th>
		</tr>
	</tbody>
</table>
                	</div>
                    
                   
			</div>
            
		</div>
		<div class="col-xs-12 col-md-4 col-lg-3">
       
			<?php include 'sidebar.php'; ?>
           
		</div>
	</div>
</div>
