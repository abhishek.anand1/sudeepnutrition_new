
<div class="container margin-top-10">
   <div class="row">
        <div class="breadcrumb">
            <?php echo $list['breadcrumb']; ?>  
        </div>
    </div>
	<div class="google-maps">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3889.8807529969417!2d77.59193618782747!3d12.850977750544251!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae6aea6f582db5%3A0xf2c0a4e367a935a4!2sT.John+College!5e0!3m2!1sen!2s!4v1467441593280" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<div class="quick-connect">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-lg-6 text-center rp-mb-10"><i class="fa fa-phone" aria-hidden="true"></i><a style=" color:#fff;" href="tel:<?php echo @$list['contactadress'][0]['contactaddress_phone'];?>"><?php echo @$list['contactadress'][0]['contactaddress_phone'];?></a></div>
			<div class="col-xs-12 col-sm-6 col-lg-6 text-center"><i class="fa fa-envelope-o" aria-hidden="true"></i><a style=" color:#fff;" href="mailto:<?php echo @$list['contactadress'][0]['contactaddress_email'];?>"><?php echo @$list['contactadress'][0]['contactaddress_email'];?></a></div>
		</div>
	</div>
	<div class="contact-content">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<!-- <h3>Corporate Address</h3>
				<?php echo $list['contactaddress'][0]['contactaddress_address'];?> -->
				<h3>Campus Address:</h3>
					<?php echo $list['contactaddress'][0]['contactaddress_campusaddress'];?>
			</div>
			<div class="col-xs-12 col-sm-6 col-lg-6">
            <?php echo $list['contactaddress'][0]['contactaddress_address'];?>
				<!-- <h3 class="rp-mt-50">Write to us</h3>
                <?php
					if($this->session->flashdata('message'))
					{
						?>
                                <p style="color:#090;"><?php echo $this->session->flashdata('message');?></p>

                            <?php
					}
					?>
				<form method="post" id="contact-us" action="<?php echo base_url()?>contact_new/send">
					<fieldset class="form-group">
						<input type="text" class="form-control" name="name" placeholder="NAME*" />
					</fieldset>
					<fieldset class="form-group">
						<input type="text" class="form-control" name="email" placeholder="EMAIL*" />
					</fieldset>
					<fieldset class="form-group">
						<input type="text" class="form-control" name="phone" placeholder="PHONE*" />
					</fieldset>
					<fieldset class="form-group">
						<input type="text" class="form-control" name="subject" placeholder="SUBJECT" />
					</fieldset>
					<fieldset class="form-group">
						<textarea rows="3" cols="30" placeholder="MESSAGE (Not exceed 160 character)" name="message" maxlength="160" class="form-control"></textarea>
					</fieldset>
					<fieldset class="form-group text-right">
                                        <script src='https://www.google.com/recaptcha/api.js'></script>
                                        <div class="g-recaptcha" data-sitekey="6Ldp1BsUAAAAAPSxj7j-245mAYHlq2f-4NSMlqS0"></div>
						<input type="submit" class="btn" value="Submit" id="submit"/>
					</fieldset> 
				</form> -->
			</div>
		</div>
	</div>
</div>
