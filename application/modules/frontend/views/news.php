<div class="container margin-top-10">
  <div class="row">
        <div class="breadcrumb">
            <?php echo $list['breadcrumb']; ?>  
        </div>
    </div>
	<div class="row">
		<div class="col-xs-12 col-md-8 col-lg-9">
			<?php /*?><div class="inner-banner">
				<img src="<?php echo base_url()?>assets/images/about-banner.jpg" width="750" height="350" alt="T John College">
			</div><?php */?>
			<div class="main-content" style="padding-bottom:0;">
				<h3></h3>
				 <?php
				foreach($list['news'] as $newsval)
		{
			?>
					<h4><a href="<?php echo base_url()?>news/details/<?php echo $newsval['news_id'];?>"><?php echo $newsval['news_title'];?></a></h4>
					
					<p><?php echo $newsval['news_shortcontent'];?></p>
<hr />
<?php
		}
		?>
			</div>
             <div class="pagination-container">
                <nav class="pagination">
                    <ul>
                        <?php foreach ($list['links'] as $link) {
							echo "<li>". $link."</li>";
							} ?>
                    </ul>
                </nav>
			</div>
		</div>
		<div class="col-xs-12 col-md-4 col-lg-3">
			<?php include 'sidebar.php'; ?>
		</div>
	</div>
</div>