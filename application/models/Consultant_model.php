<?php

class Consultant_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
<<<<<<< HEAD
=======
    
    function insert_new_consultant($data)
    {
		$validate_result = $this->validate_registration($data);
		if ($validate_result['status']) {
			$this->db->insert('consultants', $data);
			$insertId = $this->db->insert_id();
			return array('consultant_id' => $insertId);
		} else {
			return $validate_result;
		}
	}

>>>>>>> 1e8b223ac87fc809157696b4b51f6370ea88829c
	
	function update_consultant($data)
    {
		$response = array('status' => false);
		if (empty($data['id'])) {
			$response['error'] = 'Data Id is required.';
			return $response;
		}
		$validate_result = $this->validate_registration($data);
		if ($validate_result['status']) {
			$this->db->where('id', $data['id']);
        	$this->db->update('consultants', $data);
			return array('consultant_id' => $insertId);
		} else {
			return $validate_result;
		}
<<<<<<< HEAD
	}	

}
=======
	}
	
	function approve_reject_consultant($cons_id, $approved)
    {
		$response = array('status' => false);
		$update_data = null;
		if ($approved) {
			$update_data = array('is_approved' => true);
		} else {
			$update_data = array('is_deleted' => true);
		}
		
		$this->db->where('id', $cons_id);
		$this->db->update('consultants', $update_data);
		return true;
	}
	
	function get_approved_consultants()
    {
		$this->db->select('*');
		$this->db->from('consultants');
		$this->db->where('is_approved', true);
		$this->db->where('is_deleted', false);
		$query = $this->db->get();
		$consultants = $query->result();
		return array('consultants' => $consultants);
	}
	
	function get_consultant_by_id($cons_id)
    {
		$this->db->select('*');
		$this->db->from('consultants');
		$this->db->where('id', $cons_id);
		$query = $this->db->get();
		$consultant = $query->row();
		return $consultant;
	}
	function get_all_consultants()
    {
		$this->db->select('*');
		$this->db->from('consultants');
		$this->db->where('is_deleted', false);
		$query = $this->db->get();
		$consultants = $query->result();
		return array('consultants' => $consultants);
	}

	function validate_registration($data) {
		$response = array('status' => false);

		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('state', 'State', 'required');
		$this->form_validation->set_rules('pincode', 'Pincode', 'required');
		// $this->form_validation->set_rules('work_place', 'Work Place', 'required');
		$this->form_validation->set_rules('experience', 'Experience', 'required');
		$this->form_validation->set_rules('job_type', 'Job Type', 'required');
		$this->form_validation->set_rules('education_qualification', 'Education Qualification', 'required');

		$this->form_validation->set_data($data);

		if ($this->form_validation->run() == FALSE)
		{
				$response['errors'] = $this->form_validation->error_array();
				return $response;
		}
		else
		{
				$response['status'] = true;
				return $response;
		}
	}
	

}
>>>>>>> 1e8b223ac87fc809157696b4b51f6370ea88829c
