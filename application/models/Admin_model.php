<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

  public function auth_check($record){
        $this->db->select('*');
        $this->db->where('email', $record['email']);
        $this->db->where('password', $record['password']);
        $query = $this->db->get('users');
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
   }

}

