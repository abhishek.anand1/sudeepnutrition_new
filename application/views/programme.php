    <!-- /* start banner section */ -->
    <section class="position-relative" id="">
        <div class="section-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <h2><?php echo $banner['banner_heading'];?>
                            <br><strong><?php echo $banner['banner_subheading'];?></strong>
                        </h2>
                        <div class="banner_content">
                            <a href="<?php echo $banner['banner_btn'];?>" target="_blank" class="btn enquirebtn">Apply
                                Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php echo base_url().$banner['banner_img'];?>" alt="Banner" class="img-fluid w-100">
        <div class="clearfix"></div>
    </section>
    <!-- /* end banner section */ -->

    <!-- announcement start -->
    <section class="pt-2 bg4">
        <div class="container">
            <div class="d-flex">
                <h5 class="text-white pr-3">ANNOUNCEMENTS</h5>
                <marquee behavior="scroll" direction="left" scrollamount="5" onmouseover="stop();" onmouseout="start();"
                    scrolldelay="2s" class="text-white"><?php echo $banner['announcement'];?></marquee>
            </div>
        </div>
    </section>
    <!-- announcement end -->

    <!-- Program highlight -->


    <?php if (!empty($program_highlights[0])): ?>
    <section class="highlight section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center title1 font-weight-bold pb-5">Program Highlights</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php
                
            foreach ($program_highlights as $program) {
                
                ?>

                <div class="col-md-3 col-6">
                    <div class="box_highlights">
                        <img src="<?php echo base_url().$program['icon'] ;?>" alt="" class="img-fluid">
                        <p class="p1 pt-3"><?php echo $program['description'];?></p>
                    </div>
                </div>
                <?php
        }
        ?>

            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- Program Highlight End -->

    <!-- Program Details Start -->


    <?php if (!empty($program_structure[0])): ?>
    <section class="details section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title1 font-weight-bold text-center">Program Details</h1>
                    <h3 class="text-center pt-lg-4 py-3">Program Structure</h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="row justify-content-center">
                        <?php
                
                         foreach ($program_structure as $structure) {
                    
                         ?>

                        <div class="col-md-3 col-6 mb-4 px-lg-2">
                            <div class="box">
                                <h3 class="d1"><?php echo $structure['semester'];?></h3>
                                <?php 
                                
                                $sub_array=explode(',',$structure['subjects']);
                                $count=count($sub_array);
                                for($i=0;$i<$count;$i++){
                                    echo '<p>'.$sub_array[$i].'</p>';
                                }
                                ?>

                            </div>
                        </div>

                        <?php
                    }
                    ?>
                    </div>
                </div>
            </div>

            <?php if (!empty($specialization_subject[0])): ?>
            <section class="d-md-block d-none" id="programs">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="text-center pt-lg-5 py-3">Specialisation Subjects</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row no-gutters">
                                <div class="col-md-12 mb-3">
                                    <ul class="nav row nav-pills pg-link" id="myTab" role="tablist">
                                        <?php  
                                            $count = 0;              
                                            foreach ($specialization_subject as $specialization) {
                                            ?>
                                        <li class="nav-item col">
                                            <a class="nav-link <?= $count == 0 ? 'active' : '' ?>" id="pgdc<?= $count?>"
                                                data-toggle="tab" href="#one<?= $count?>" role="tab"
                                                aria-controls="pgdc<?= $count?>" aria-selected="true">
                                                <?php echo $specialization['specialization'];?>
                                            </a>
                                        </li>
                                        <?php
                                        $count++;
                                            }
                                        ?>
                                    </ul>
                                </div>
                                <div class="col-md-12 pl-md-0">
                                    <div class="tab-content" id="myTabContent">
                                        <?php  
                                            $count = 0;              
                                            foreach ($specialization_subject as $specialization) {
                                                
                                            ?>

                                        <div class="tab-pane fade <?= $count == 0 ? 'show active' : '' ?>"
                                            id="one<?= $count?>" role="tabpanel" aria-labelledby="pgdc<?= $count?>">
                                            <div class="row justify-content-between">
                                                <div class="col-md-5 col-6">
                                                    <div class="box1">
                                                        <h6 class="t1">
                                                            <?php echo $specialization['specialization_subject_1'];?>
                                                        </h6>
                                                        <?php 
                                
                                                                $sub_array=explode(',',$specialization['specialization_pref_1']);
                                                                $count1=count($sub_array);
                                                                for($i=0;$i<$count1;$i++){
                                                                    echo '<p class="p">'.$sub_array[$i].'</p>';
                                                                }
                                                                ?>


                                                        <h6 class="t1">
                                                            <?php echo $specialization['elective_subject_1'];?>
                                                        </h6>
                                                        <?php 
                                
                                                            $sub_array=explode(',',$specialization['elective_pref_1']);
                                                            $count2=count($sub_array);
                                                            for($i=0;$i<$count2;$i++){
                                                                echo '<p class="p">'.$sub_array[$i].'</p>';
                                                            }
                                                            ?>

                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-6">
                                                    <div class="box1">
                                                        <h6 class="t1">
                                                            <?php echo $specialization['specialization_subject_2'];?>
                                                        </h6>
                                                        <?php 
                                
                                                            $sub_array=explode(',',$specialization['specialization_pref_2']);
                                                            $count3=count($sub_array);
                                                            for($i=0;$i<$count3;$i++){
                                                                echo '<p class="p">'.$sub_array[$i].'</p>';
                                                            }
                                                            ?>


                                                        <h6 class="t1">
                                                            <?php echo $specialization['elective_subject_2'];?>
                                                        </h6>
                                                        <?php 
                                
                                                            $sub_array=explode(',',$specialization['elective_pref_2']);
                                                            $count4=count($sub_array);
                                                            for($i=0;$i<$count4;$i++){
                                                                echo '<p class="p">'.$sub_array[$i].'</p>';
                                                            }
                                                            ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $count++;

                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>
            <?php endif; ?>
        </div>
    </section>
    <?php endif; ?>

    <?php if (!empty($specialization_subject[0])): ?>
    <section class="position-relative faq d-md-none d-block">
        <div id="mobile_acc" class="col-md-12 d-md-none d-block px-md-3 px-0">
            <div class="accordion" id="faq">
                <?php  
                // var_dump($specialization_subject[0]);
                // die;
                                            $count0 = 0;              
                                            foreach ($specialization_subject as $specializations) {
                                            ?>
                <div class="card">
                    <div class="card-header" id="faqhead<?= $count0?>">
                        <a href="#" class="btn btn-header-link <?= $count == 0 ? 'show' : 'collapsed' ?>"
                            data-toggle="collapse" data-target="#faq<?= $count0?>" aria-expanded="true"
                            aria-controls="faq<?= $count0?>">
                            <?php echo $specializations['specialization'];?></a>
                    </div>

                    <div id="faq<?= $count0?>" class="collapse  <?= $count == 0 ? 'show active' : '' ?>"
                        aria-labelledby="faqhead<?= $count0?>" data-parent="#faq">
                        <div class="card-body">
                            <div class="row justify-content-between">
                                <div class="col-md-5 col-6 px-md-3 px-1">
                                    <div class="box1">
                                        <h6 class="t1">
                                            <?php echo $specializations['specialization_subject_1'];?>
                                        </h6>
                                        <?php 
                                
                                                                $sub_array=explode(',',$specializations['specialization_pref_1']);
                                                                $count1=count($sub_array);
                                                                for($i=0;$i<$count1;$i++){
                                                                    echo '<p class="p">'.$sub_array[$i].'</p>';
                                                                }
                                                                ?>


                                        <h6 class="t1">
                                            <?php echo $specializations['elective_subject_1'];?>
                                        </h6>
                                        <?php 
                                
                                                            $sub_array=explode(',',$specializations['elective_pref_1']);
                                                            $count2=count($sub_array);
                                                            for($i=0;$i<$count2;$i++){
                                                                echo '<p class="p">'.$sub_array[$i].'</p>';
                                                            }
                                                            ?>
                                    </div>
                                </div>
                                <div class="col-md-5 col-6 px-md-3 px-1">
                                    <div class="box1">
                                        <h6 class="t1">
                                            <?php echo $specializations['specialization_subject_2'];?>
                                        </h6>
                                        <?php 
                                
                                                            $sub_array=explode(',',$specializations['specialization_pref_2']);
                                                            $count3=count($sub_array);
                                                            for($i=0;$i<$count3;$i++){
                                                                echo '<p class="p">'.$sub_array[$i].'</p>';
                                                            }
                                                            ?>


                                        <h6 class="t1">
                                            <?php echo $specializations['elective_subject_2'];?>
                                        </h6>
                                        <?php 
                                
                                                            $sub_array=explode(',',$specializations['elective_pref_2']);
                                                            $count4=count($sub_array);
                                                            for($i=0;$i<$count4;$i++){
                                                                echo '<p class="p">'.$sub_array[$i].'</p>';
                                                            }
                                                            ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                                        $count0++;

                                            }
                                        ?>

            </div>
        </div>
        <div class="clearfix"></div>
    </section>
    <?php endif; ?>

    <!-- Program Details END -->

    <!-- Career Start -->
    <?php if (!empty($career_option[0])): ?>
    <section class="career section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center title1 font-weight-bold  pb-5">Career Options</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 mx-auto">
                    <div class="row justify-content-center text-center">
                        <?php
                
                         foreach ($career_option as $career) {
                
                        ?> <div class="col-md-2 mb-lg-5 mb-4">
                            <img src="<?php echo base_url().$career['icon'] ;?>" alt="" class="img-fluid">
                            <p><?php echo $career['description'];?></p>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

    </section>
    <?php endif; ?>

    <!-- Career End -->

    <section class="eligibility section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="pb-md-5 title1 font-weight-bold ">Eligibility</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?php
                foreach ($eligibility as $eligible) {
               ?>
                    <?php 
                                
                                $criterias=explode(',',$eligible['criteria']);
                                $count=count($criterias);
                                for($i=0;$i<$count;$i++){
                                     $url=base_url();
                                    echo '<img src="'.$url.'assets/img/SCDL Programme Details/tick.svg"
                    alt=""
                    class="img-fluid"><p>'.$criterias[$i].'</p>';
                    }
                    ?>
                </div>

                <div class="col-md-8">
                    <h4 class="font-weight-bold text_red mb-lg-3 mb-2">Fees</h4>
                    <div class="table-responsive">
                        <table class="table bg2 table-bordered text-center p-lg-3 p-3">
                            <tbody>
                                <tr>
                                    <th colspan="3">
                                        General
                                    </th>
                                    <th colspan="3">
                                        Defence
                                    </th>
                                </tr>
                                <tr>
                                    <th rowspan="2">
                                        Full fee One Time
                                    </th>
                                    <th colspan="2">
                                        Installment
                                    </th>
                                    <th rowspan="2">
                                        Full fee One Time
                                    </th>
                                    <th colspan="2">
                                        Installment
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        1st
                                    </th>
                                    <th>
                                        2nd
                                    </th>
                                    <th>
                                        1st
                                    </th>
                                    <th>
                                        2nd
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <p>₹ <?= $eligible['General_fees_one_time'];?></p>
                                    </td>
                                    <td>
                                        <p>₹ <?= $eligible['General_fees_installment_1'];?></p>
                                    </td>
                                    <td>
                                        <p>₹ <?= $eligible['General_fees_installment_2'];?></p>
                                    </td>
                                    <td>
                                        <p>₹ <?= $eligible['defence_fees_one_time'];?></p>
                                    </td>
                                    <td>
                                        <p>₹ <?= $eligible['defence_fees_installment_1'];?></p>
                                    </td>
                                    <td>
                                        <p>₹ <?= $eligible['defence_fees_installment_2'];?></p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                    <?php if (!empty($eligible['nri_fees_one_time'])):  ?>

                    <div class="table-responsive">
                        <table class="table table-bordered bg2 text-center p-lg-3 p-3">
                            <tbody>
                                <tr>
                                    <th colspan="3">
                                        NRI
                                    </th>
                                    <th colspan="3">
                                        SAARC
                                    </th>
                                </tr>
                                <tr>
                                    <th rowspan="2">
                                        Full fee One Time
                                    </th>
                                    <th colspan="2">
                                        Installment
                                    </th>
                                    <th rowspan="2">
                                        Full fee One Time
                                    </th>
                                    <th colspan="2">
                                        Installment
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        1st
                                    </th>
                                    <th>
                                        2nd
                                    </th>
                                    <th>
                                        1st
                                    </th>
                                    <th>
                                        2nd
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <p>$ <?= $eligible['nri_fees_one_time'];?></p>
                                    </td>
                                    <td>
                                        <p>$ <?= $eligible['nri_fees_installment_1'];?></p>
                                    </td>
                                    <td>
                                        <p>$ <?= $eligible['nri_fees_installment_2'];?></p>
                                    </td>
                                    <td>
                                        <p>$ <?= $eligible['saarc_fees_one_time'];?></p>
                                    </td>
                                    <td>
                                        <p>$ <?= $eligible['saarc_fees_installment_1'];?></p>
                                    </td>
                                    <td>
                                        <p>$ <?= $eligible['saarc_fees_installment_2'];?></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php endif; ?>
                    <p class="mt-lg-3 my-2 text_red"><strong>Notes</strong></p>
                    <ul class="pl-3">

                        <?php 
                                
                                $notes=explode(',',$eligible['notes']);
                                $count=count($notes);
                                for($i=0;$i<$count;$i++){
                                
                                    echo '<li>'.$notes[$i].'</li>';
                    }
                    ?>


                    </ul>
                    <?php
               }
               ?>


                </div>
    </section>

    <!-- Career End -->

    <!-- How to apply -->

    <section class="apply_section section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center title1 text-white font-weight-bold ">How to apply</h1>
                    <p class="text-center pa"><a href="www.scdl.net" target="_blank" class="text-white"> Visit
                            www.scdl.net to Apply Online:</a></p>
                </div>
            </div>
            <div class="row pt-4 text-center">
                <div class="col-md-4 ">
                    <span class="circle1">1</span>
                    <p class="a1 text-center">Purchase and fill application<br> form online</p>
                </div>
                <div class="col-md-4">
                    <span class="circle1">2</span>
                    <p class="a1 text-center">Submit copies of documents in prescribed format as per eligibility
                        of
                        applied program.</p>
                </div>
                <div class="col-md-4">
                    <span class="circle1">3</span>
                    <p class="a1 text-center">Pay program fee online through<br> student login</p>
                </div>
            </div>
        </div>
    </section>

    <!-- How to apply End -->