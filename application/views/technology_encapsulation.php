<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sudeep Nutrition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/icons/fav.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/responsive.css">
</head>



<!-- Humberger Navbar -->

<?php include('header.php');?>


<!-- Banner -->

<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="images/aboutus/banner.jpg" class="img-fluid w-100 d-md-block d-none">
                    <img src="images/aboutus/banner-1.jpg" class="img-fluid w-100 d-block d-lg-none">
                    <div class="banner_text">
                        <h3 class="text-white" style="font-size: 3rem;font-weight: bold;">Encapsulation</h3>
                        <div class="primary-button pt-3">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>  
</section> 

<!-- End of Banner -->

</section class="info-section">
    <div class="container px-0">
        <div class="row no-gutters">
        <div class="col-lg-12">
                <h2 class="info color-green">Encapsulation Technology is a process of entrapping a core material (active substance) within a secondary material that protects from the environment and that can deliver the active substance to the specific site.</h2>
            </div>
            <div class="col-lg-12">
                <img src="images/technology/infographic.png" class="img-fluid">
            </div>
        </div> 
    </div>  
<section>

<section class="info-section">
    <div class="container px-0">
        <div class="row no-gutters">
            <div class="col-lg-12">
               <h1 class="encap-text">Why Encapsulate?</h1>
            </div>
            <div class="col-lg-12 pb-5">
               <div class="row">
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/1.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/2.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/3.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/4.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/5.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/6.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/7.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/8.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <!-- <img src="images/technology/9.svg" class="img-fluid img-bg"> -->
                                <!-- <p class="text-p">To enhance the stability of active substances.</p> -->
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/9.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <img src="images/technology/10.svg" class="img-fluid img-bg">
                                <p class="text-p">To enhance the stability of active substances.</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="row">
                           <div class="col-lg-12 encap-div">
                                <!-- <img src="images/technology/9.svg" class="img-fluid img-bg"> -->
                                <!-- <p class="text-p">To enhance the stability of active substances.</p> -->
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div> 
    </div>  
</section>

<?php include('footer.php');?>

</body>

</html>