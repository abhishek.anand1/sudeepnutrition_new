<!-- /* footer */ -->
<div class="container px-0">
    <div class="row no-gutters justify-content-center top-part">
        <div class="col-md-6 text-center center-content">
            <div class="d-flex-inline">
                <!-- <h3 class="footer-heading">Start a Project with itCraft</h3> -->
                <img src="images/footer logo.png" alt="" class="img-fluid">
            </div>
            <div class="row justify-content-center mt-3">
                <img src="images/social media 5.svg" alt="" class="img-fluid">
                <img src="images/social media 4.svg" alt="" class="img-fluid">
                <img src="images/social media 3.svg" alt="" class="img-fluid">
                <img src="images/social media 2.svg" alt="" class="img-fluid">
                <img src="images/social media 1.svg" alt="" class="img-fluid">
                <!-- <div class="col-md-1">
                    <img src="images/social media 5.svg" alt="" class="img-fluid">
                </div>
                <div class="col-md-1">
                    <img src="images/social media 4.svg" alt="" class="img-fluid">
                </div>
                <div class="col-md-1">
                    <img src="images/social media 3.svg" alt="" class="img-fluid">
                </div>
                <div class="col-md-1">
                    <img src="images/social media 2.svg" alt="" class="img-fluid">
                </div>
                <div class="col-md-1">
                    <img src="images/social media 1.svg" alt="" class="img-fluid">
                </div> -->
            </div>
            <hr>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="line mb-3 mx-auto"></div>
        <div class="d-md-flex justify-content-around  col-md-12 pt-5 pb-5 mb-3">
            <div class="flex-fill bd-highlight mb-5 mb-md-0">
                <h3 class="f1">About us</h3>
                <div class="para">
                    <p>What we do</p>
                    <p>What Sevices we provide</p>
                    <p>Our Mission</p>
                    <p>Innovation</p>
                    <p>Responsibility (CSR)</p>
                    <p>Board Of Directors</p>
                    <p>Join Our team</p>
                </div>
            </div>
            <div class="flex-fill bd-highlight mb-3 mb-md-0">
                <h3 class="f1">Technologies</h3>
                <div class="para">
                    <p>Micro Encapsulation</p>
                    <p>Hot Melt Extrusion</p>
                    <p>Drying</p>
                    <p>Blending</p>
                    <p>Granulation</p>
                </div>
            </div>
            <div class="flex-fill bd-highlight mb-3 mb-md-0">
                <h3 class="f1">Markets</h3>
                <div class="para">
                    <p>Bakery and Cereal Products</p>
                    <p>Savoury and Snack Products</p>
                    <p>Dairy Products</p>
                    <p>Beverages Meat and Meat Products</p>
                    <p>Confectionery Products</p>
                    <p>Infant Nutrition</p>
                </div>
            </div>
            <div class="flex-fill bd-highlight mb-3 mb-md-0">
                <h3 class="f1">Quality & product safety</h3>
                <div class="para">
                    <p> Accreditations</p>
                    <p>Certifications</p>
                    <p>Compliances</p>
                </div>

            </div>

            <div class="flex-fill bd-highlight mb-3 mb-md-0">
                <h3 class="f1">Resource Centre</h3>
                <div class="para">
                    <p>Request A Sample</p>
                    <p>Your Application Expert Product Development</p>
                    <p>Product Information Services</p>
                </div>
                <div class="row">
                    <div class="p-4 flex-fill bd-highlight">
                        <h3 class="f1">Media</h3>
                        <div class="para">
                            <p> Articles </p>
                            <p>What’s in the News? </p>
                            <p>Events </p>
                            <p>Press Release</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="line mb-3 mx-auto"></div>
    </div>

    <div class="row no-gutters color-grey  pt-2">
        <div class="col-md-6">
            <h6 style="font-size: 0.8rem;">© 2020 Mockup. All Rights Reserved.</h6>
        </div>
        <div clas="col-md-6">
            <h6 style="font-size: 0.8rem;">Privacy Policy Terms of Service</h6>
        </div>
    </div>

</div>
<button class="btn scroll" id="scroll1" style="display: none;">Request a Sample</button>

<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url();?>assets/css/carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/js/scripts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>


</body>

</html>