<nav class="navbar  navbar-light navbar-expand-lg w-100" id=header>
    <div class="container px-0">
        <a class="navbar-brand float-md-left" href="#">
            <img src="<?php echo base_url(); ?>assets/images/header logo.png" class="img-fluid">
        </a>
        <!-- <button class="navbar-toggler float-right mt-1" type="button" data-toggle="collapse"
            data-target="#collapsibleNavbar">
        </button> -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav  ml-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="#" style="font-weight: bold;font-size: 1.6rem;">Technologies</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active " href="#" href="#"
                        style="font-weight: bold;font-size: 1.6em;">Markets</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="#" href="#"
                        style="font-weight: bold;font-size: 1.6em;">Products</a>
                </li>
            </ul>

        </div>

        <!-- <a class="navbar-brand  m1 d-none d-lg-block " href="#">
            
            <img src="assets/images/hamburger menu.svg" class="img-fluid">

        </a> -->
        <div id="mySidepanel" class="sidepanel">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
            <div class="dropdown">
                <a href="#home" class="dropdown-btn">About Us <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="dropdown-content">
                    <a class="nav-subTitle" href="#" title="">What we do</a>
                    <a class="nav-subTitle" href="#" title="">What Sevices we provide</a>
                    <a class="nav-subTitle" href="#" title="">Our Mission</a>
                    <a class="nav-subTitle" href="#" title="">Innovation</a>
                    <a class="nav-subTitle" href="#" title="">Responsibility (CSR)</a>
                    <a class="nav-subTitle" href="#" title="">Board Of Directors</a>
                    <a class="nav-subTitle" href="#" title="">Join Our team</a>
                </div>
            </div>
            <div class="dropdown">
                <a href="#home" class="dropdown-btn">Resource Centre<i class="fa fa-angle-down"
                        aria-hidden="true"></i></a>
                <div class="dropdown-content">
                    <a class="nav-subTitle" href="#" title="">Request A Sample</a>
                    <a class="nav-subTitle" href="#" title="">Your Application Expert Product Development</a>
                    <a class="nav-subTitle" href="#" title="">Product Information Services</a>
                </div>
            </div>
            <div class="dropdown">
                <a href="#home" class="dropdown-btn">Quality & product safety<i class="fa fa-angle-down"
                        aria-hidden="true"></i></a>
                <div class="dropdown-content">
                    <a class="nav-subTitle" href="#" title="">Accreditations</a>
                    <a class="nav-subTitle" href="#" title="">Certifications</a>
                    <a class="nav-subTitle" href="#" title="">Compliances</a>
                </div>
            </div>
            <div class="dropdown">
                <a href="#home" class="dropdown-btn">Media<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="dropdown-content">
                    <a class="nav-subTitle" href="#" title="">Articles</a>
                    <a class="nav-subTitle" href="#" title="">What’s in the News?</a>
                    <a class="nav-subTitle" href="#" title="">Events</a>
                    <a class="nav-subTitle" href="#" title="">Press Release</a>
                </div>
            </div>
            <div class="dropdown">
                <a href="#home" class="btn_sec1">Contact Us</a>
            </div>
        </div>





        <!-- <button class="dropdown-btn">About Us<i class="fa fa-angle-down" aria-hidden="true"></i></button> -->
        <!-- <button class="dropdown-btn">Resource Centre<i class="fa fa-angle-down" aria-hidden="true"></i></button>
            <button class="dropdown-btn">Quality & product safety<i class="fa fa-angle-down"
                    aria-hidden="true"></i></button>
            <button class="dropdown-btn">Media<i class="fa fa-angle-down" aria-hidden="true"></i></button>
            <a href="#home" class="btn_sec">Contact <span>→</span></a> -->
            <button class="openbtn" onclick="openNav()"><img src="<?php echo base_url(); ?>assets/images/hamburger menu.svg"
            class="img-fluid d-md-block d-none"></button>
            <!-- <button class="openbtn" onclick="openNav()"><img src="images/hamburger menu.svg"
            class="img-fluid d-block d-lg-none"></button> -->

    </div>

    <!-- <div id="main">
            <button class="openbtn" onclick="openNav()"><img src="images/hamburger menu.svg" class="img-fluid"></button>
            
        </div> -->
    <script>
    function openNav() {
        document.getElementById("mySidepanel").style.width = "350px";
    }

    function closeNav() {
        document.getElementById("mySidepanel").style.width = "0";
    }

    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
                dropdownContent.style.display = "none";
            } else {
                dropdownContent.style.display = "block";
            }
        });
    }
    // window.onscroll = function() {
    //     myFunction()
    // };
    </script>
    </div>
</nav>