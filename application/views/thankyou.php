    <!-- /* start banner section */ -->
    <section class="position-relative" id="">
        <img src="<?php echo base_url();?>assets/img/banner.jpg" alt="Banner" class="img-fluid w-100">
        <div class="section-content thankyou">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="p-4 bg-white text-success w-auto">
                            <h3>Thank you for your Enquiry</h3>
                            <p>We will get back to you soon!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </section>
    <!-- /* end banner section */ -->

    <!-- /* start about */ container-->
    <section class="section_padding" id="about">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4 mb-md-0 mb-4">
                    <img src="<?php echo base_url();?>assets/img/img1.jpg" alt="" class="img-fluid w-100">
                </div>
                <div class="col-md-7 ml-auto">
                    <h1 class="main-title">SCDL</h1>
                    <h4>INDIA’S NO. 1 DISTANCE LEARNING INSTITUTE</h4>
                    <p>Incepted in 2001, the Symbiosis Centre for Distance Learning (SCDL) has continuously made rapid
                        strides in providing exemplary education making it one of the largest autonomous distance
                        learning education institute in India. We offer various Post Graduate Diploma, Diploma and
                        Certificate programs across various industry sectors that include Business Management,
                        International Business, Information Technology, Banking and Finance, Entrepreneurship
                        Development among others.</p>
                    <p>SCDL has, for the first time in India, introduced online assessment systems, to pursue our
                        mission of 'flexible learning opportunities anywhere, anytime' and to provide ultimate
                        convenience, ease and flexibility to our students.</p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
    <!-- /* end about */ -->

    <!-- /* start programs offered */ -->
    <section class="programs-bg section_padding" id="programs">
        <div class="container">
            <h1 class="font-weight-bold">Programmes Offered</h1>
            <div class="row">
                <div class="col-md-12 d-md-block d-none">
                    <div class="row no-gutters">
                        <div class="col-md-12 mb-3">
                            <h5 class="subtitle mt-4">Select Course</h5>
                            <ul class="nav row nav-pills pg-link" id="myTab" role="tablist">
                                <li class="nav-item col">
                                    <a class="nav-link active" id="pgdc-tab" data-toggle="tab" href="#one" role="tab"
                                        aria-controls="pgdc" aria-selected="true">POST GRADUATION DIPLOMA COURSES</a>
                                </li>
                                <li class="nav-item col">
                                    <a class="nav-link" id="dp-tab" data-toggle="tab" href="#two" role="tab"
                                        aria-controls="dp" aria-selected="true">DIPLOMA PROGRAMS</a>
                                </li>
                                <li class="nav-item col">
                                    <a class="nav-link" id="pgcp-tab" data-toggle="tab" href="#three" role="tab"
                                        aria-controls="pgcp" aria-selected="true">POST GRADUATION CERTIFICATE
                                        PROGRAMS</a>
                                </li>
                                <li class="nav-item col">
                                    <a class="nav-link" id="cc-tab" data-toggle="tab" href="#four" role="tab"
                                        aria-controls="cc" aria-selected="true">CERTIFICATE COURSES</a>
                                </li>

                            </ul>
                        </div>
                        <div class="col-md-12 pl-md-0">
                            <div class="tab-content" id="myTabContent">
                                <h5 class="subtitle my-4">Courses and Specialization</h5>

                                <div class="tab-pane fade show active" id="one" role="tabpanel"
                                    aria-labelledby="pgdc-tab">
                                    <div class="row course_list">

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_1" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Business Administration</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_2" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Supply Chain Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_3" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">IT</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_4" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Banking & Financial Services</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_5" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">International Business</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_6" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Insurance Business Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_7" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Retail Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_8" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Pre-primary Teachers Training</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_9" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Human Resource Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_10" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Customer Relationship Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_11" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Export & Import Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_12" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Business & Corporate Law</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_13" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Technical Writing in Business Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_14" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Instructional Design</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_15" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Corporate PGDBA</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_16" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Educational Administration</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_17" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Data Science</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_18" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Project Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_19" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Personnel & HRM [Labour Laws]</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_20" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Energy Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_21" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Design Thinking</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_22" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">School Counselling</h5>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>

                                <div class="tab-pane fade" id="two" role="tabpanel" aria-labelledby="dp-tab">
                                    <div class="row course_list">
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_23" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Diploma In English Language Teaching</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_24" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Diploma in Creative Writing In English</h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade show" id="three" role="tabpanel" aria-labelledby="dp-tab">
                                    <div class="row course_list">
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_26" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Entrepreneurship
                                                        Development</h5>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_28" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Digital
                                                        Marketing</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_30" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Event Management</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_25" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Cyber Laws</h5>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_31" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Taxation Laws</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_27" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Management
                                                        Accounting</h5>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_29" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Single Courses</h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade show" id="four" role="tabpanel" aria-labelledby="dp-tab">
                                    <div class="row course_list">
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_32" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Intellectual Property Rights
                                                    </h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_33" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Virtual Workplace Management
                                                    </h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_35" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Blockchain Technology</h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- mobile programme -->
                <div id="mobile_acc" class="col-md-12 d-md-none d-block">
                    <div class="accordion" id="faq">
                        <div class="card">
                            <div class="card-header" id="faqhead1">
                                <a href="#" class="btn btn-header-link" data-toggle="collapse" data-target="#faq1"
                                    aria-expanded="true" aria-controls="faq1">POST
                                    GRADUATION DIPLOMA COURSES</a>
                            </div>

                            <div id="faq1" class="collapse show" aria-labelledby="faqhead1" data-parent="#faq">
                                <div class="card-body">
                                    <div class="row course_list">

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_1" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Business Administration</h5>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_2" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Supply Chain Management</h5>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_3" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">IT</h5>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_4" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Banking & Financial Services
                                                    </h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_5" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">International Business</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_6" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Insurance Business Management
                                                    </h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_7" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Retail Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_8" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Pre-primary Teachers Training
                                                    </h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_9" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Human Resource Management
                                                    </h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_10" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Customer Relationship
                                                        Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_11" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Export & Import Management
                                                    </h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_12" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Business & Corporate Law</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_13" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Technical Writing in Business
                                                        Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_14" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Instructional Design</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_15" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Corporate PGDBA</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_16" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Educational
                                                        Administration</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_17" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Data Science</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_18" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Project Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_19" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Personnel & HRM [Labour
                                                        Laws]</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_20" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Energy Management</h5>
                                                </div>
                                            </a>
                                        </div>


                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_21" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Design Thinking</h5>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_22" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">School Counselling</h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faqhead2">
                                <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse"
                                    data-target="#faq2" aria-expanded="true" aria-controls="faq2">DIPLOMA
                                    PROGRAMS</a>
                            </div>
                            <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">
                                <div class="card-body">
                                    <div class="row course_list">
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_23" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Diploma In English
                                                        Language Teaching</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_24" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Diploma in Creative
                                                        Writing In English</h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faqhead3">
                                <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse"
                                    data-target="#faq3" aria-expanded="true" aria-controls="faq3">POST
                                    GRADUATION CERTIFICATE
                                    PROGRAMS</a>
                            </div>
                            <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">
                                <div class="card-body">
                                    <div class="row course_list">
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_26" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Entrepreneurship
                                                        Development</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_28" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Digital Marketing</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_30" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Event Management</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_25" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Cyber Laws</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_31" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Taxation Laws</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_27" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Management Accounting
                                                    </h5>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_29" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Single Courses</h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="faqhead4">
                                <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse"
                                    data-target="#faq4" aria-expanded="true" aria-controls="faq4">CERTIFICATE
                                    COURSES</a>
                            </div>
                            <div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">
                                <div class="card-body">
                                    <div class="row course_list">
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_32" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Intellectual Property
                                                        Rights</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_33" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Virtual Workplace
                                                        Management</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 mb-4 col-6">
                                            <a href="programme?program_id=program_35" class="card shadow course-card">
                                                <div class="card-body ">
                                                    <h5 class="card-title">Blockchain Technology
                                                    </h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
    <!-- /* end programs offered */ -->

    <!-- /* start why scdl */ -->
    <section id="why_scdl" class=" why-scdl-bg section_padding">
        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <h1 class="text-center font-weight-bold py-4">Why SCDL</h1>
                    <div class="row no-gutters">

                        <div class="col-lg-4 col-6">
                            <div class="why_box borderB borderR">
                                <img src="<?php echo base_url();?>assets/img/1.svg" class="img-fluid">
                                <p class="mt-4">Placement assistance offered</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6">
                            <div class="why_box borderB borderR">
                                <img src="<?php echo base_url();?>assets/img/2.svg" class="img-fluid">
                                <p class="mt-4">High acceptance in the industry</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6">
                            <div class="why_box borderB">
                                <img src="<?php echo base_url();?>assets/img/3.svg" class="img-fluid">
                                <p class="mt-4">Wide range of programmes accross in demand domains</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6 ">
                            <div class="why_box borderR">
                                <img src="<?php echo base_url();?>assets/img/4.svg" class="img-fluid">
                                <p class="mt-4">Dynamic assesment system</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6 ">
                            <div class="why_box borderR">
                                <img src="<?php echo base_url();?>assets/img/5.svg" class="img-fluid">
                                <p class="mt-4">Excellent student support services</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-6 ">
                            <div class="why_box ">
                                <img src="<?php echo base_url();?>assets/img/6.svg" class="img-fluid">
                                <p class="mt-4">#1 among top Distance Learning Institutes for Student Learning
                                    Experience (Career 360)</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
    <!-- /* end why scdl */ -->

    <!-- /* start stats section */ -->
    <section class="stats py-5" id="counter">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm col-6 bordright">
                    <img src="<?php echo base_url();?>assets/img/stats 1.svg" alt="">
                    <p class=""><span class="counter_value" data-count="120000"> 120000+</span> <br> Students</p>
                </div>
                <div class="col-sm col-6 bordright">
                    <img src="<?php echo base_url();?>assets/img/stats 2.svg" alt="">
                    <p class=""><span class="counter_value" data-count="5000"> 5000+</span> <br> Collaborations</p>
                </div>
                <div class="col-sm col-6 bordright">
                    <img src="<?php echo base_url();?>assets/img/stats 3.svg" alt="">
                    <p class=""><span class="counter_value" data-count="47"> 47+</span> <br> Nationalities</p>
                </div>
                <div class="col-sm col-6 bordright">
                    <img src="<?php echo base_url();?>assets/img/stats 4.svg" alt="">
                    <p class=""><span class="counter_value" data-count="1600"> 1600+</span> <br>Expert Faculty</p>
                </div>
                <div class="col-sm col-6">
                    <img src="<?php echo base_url();?>assets/img/stats 5.svg" alt="">
                    <p class=""> <span class="counter_value" data-count="100">100+</span> <br>Programmes</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
    <!-- /* end stats section */ -->

    <!-- /* start infographic */ -->
    <section class="bg-light section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 vcntr col-xs-6 my-auto">
                    <h1>Learning @SCDL</h1>
                </div>
                <div class="col-md-8">
                    <img src="<?php echo base_url();?>assets/img/infographic.png" alt="" class="img-fluid w-100">
                </div>
            </div>
        </div>
    </section>
    <!-- /* end infographic */ -->

    <section class="section_padding awards">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center font-weight-bold text-white">Awards</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 move10">
                    <div class="card-deck">
                        <div class="card shadow">
                            <img class="card-img-top img-fluid" src="<?php echo base_url();?>assets/img/awards 1.jpg"
                                alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">Symbiosis Centre for Distance Learning has been ranked 1st amongst
                                    the top Distance Learning Institutes.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 move5">
                    <div class="card-deck">
                        <div class="card shadow">
                            <img class="card-img-top img-fluid" src="<?php echo base_url();?>assets/img/awards 2.jpg"
                                alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">Symbiosis Centre for Distance Learning has been ranked 2nd amongst
                                    the Leading Institutes.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 move10">
                    <div class="card-deck">
                        <div class="card shadow">
                            <img class="card-img-top img-fluid" src="<?php echo base_url();?>assets/img/awards 3.jpg"
                                alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">Symbiosis Centre for Distance Learning has been ranked 3rd amongst
                                    the Top Distance Learning Institutes.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 move5">
                    <div class="card-deck">
                        <div class="card shadow">
                            <img class="card-img-top img-fluid" src="<?php echo base_url();?>assets/img/awards .jpg"
                                alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">Symbiosis Centre for Distance Learning has been ranked #1 amongst
                                    top Distance Learning Institutes for Student Learning Experience (Career 360).</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- /* start Success stories */ -->
    <section class="section_padding bg-light" id="story">
        <div class="container">
            <h1 class="success-header">Success Stories</h1>
            <div class="row">
                <div class="col-md-12 mx-auto">
                    <div class="owl-carousel owl-theme" id="success_stories">
                        <div class="item">
                            <div class="card-deck">
                                <div class="card shadow success-card">
                                    <div class="card-body">
                                        <img class="story-img img-fluid"
                                            src="<?php echo base_url();?>assets/img/test1.png" alt="">
                                        <h5 class="card-title">Rajesh Nagarkar</h5>
                                        <h6 class="card-subtitle mb-2 text-muted">Project Manager, Tieto</h6>
                                        <h6 class="title-color">PGDBF</h6>
                                        <p class="card-text">I am an engineering graduate currently working as Project
                                            Manager in an IT company in the Finance domain. The post graduate course in
                                            Banking has given me knowledge and skill as per industry standards. With
                                            facilities like online booking for exams can enrol and study without
                                            affecting my current job. Symbiosis has worldwide recognition which will
                                            help me throughout my career.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="card-deck">
                                <div class="card shadow success-card">
                                    <div class="card-body">
                                        <img class="story-img img-fluid"
                                            src="<?php echo base_url();?>assets/img/test2.png" alt="">
                                        <h5 class="card-title">Piyush Chavda</h5>
                                        <h6 class="card-subtitle mb-2 text-muted">SME-UNIX</h6>
                                        <h6 class="title-color">HP India Sales P Ltd</h6>
                                        <p class="card-text">I have enrolled for the PGDBA Program at Symbiosis Center
                                            for Distance Learning. The elaborate course material, facility to have
                                            e-learning sessions, live chat room and not to forget the virtual classrooms
                                            helps one to prepare in totality, thus enabling you to gain an in-depth
                                            understanding of the subject. I feel most content and would like to
                                            appreciate the freedom that one gets to schedule their exams online. The
                                            flexibility that the program offers to study at your own time, fits
                                            perfectly into my busy work schedule in spite of spending long hours at the
                                            office. Above all, Symbiosis has a good recognition and acceptability
                                            amongst employers and this helps one to progress in their career.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="card-deck">
                                <div class="card shadow success-card">
                                    <div class="card-body">
                                        <img class="story-img img-fluid"
                                            src="<?php echo base_url();?>assets/img/test3.png" alt="">
                                        <h5 class="card-title">Utpal Oza</h5>
                                        <h6 class="card-subtitle mb-2 text-muted">Head Learning & Development</h6>
                                        <h6 class="title-color">Reliance Industries Ltd.</h6>
                                        <p class="card-text">Symbiosis Center for Distance Learning is one of the
                                            reputed institutes for Management Studies in India. Through its excellent
                                            faculty, teaching methods and courses, it has been helping thousands of
                                            students to achieve their goals with respect to higher education. It has
                                            several innovative facilities such as the Virtual Classroom, Online Faculty
                                            Chat, On-demand examinations etc. that enable students from across the globe
                                            to manage both their career and education. As part of my role in the
                                            Resource Management Group, I interact extensively with associates and
                                            understand their aims and aspirations while mapping them to a role. A
                                            detailed knowledge of Human Resource Management is required for a better
                                            insight into managing the various challenges faced in my role. I believe
                                            that a Post Graduate Diploma in Human Resource Management from SCDL has
                                            equipped me with the knowledge and expertise to perform to the best of my
                                            abilities.</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>

        <div class="clearfix"></div>
    </section>
    <!-- /* end Success stories */ -->