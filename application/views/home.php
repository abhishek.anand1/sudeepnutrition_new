
<!-- Humberger Navbar -->

<!--====== banner start ==========-->
<section class="banner-section position-relative" id="home">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/banner.jpg" class="img-fluid w-100 d-md-block d-none">
                    <img src="<?php echo base_url(); ?>assets/images/banner1.jpg" class="img-fluid w-100 d-block d-lg-none">



                    <div class="banner_text">
                        <h3><span class="text_white">Delivering </span><span class="text_red">better
                                nutrition</span>
                            <span class="text_white">for <br>every step of life’s journey</span>
                        </h3>
                        <div class="primary-button pt-3">
                            <a href="#">Enquire Now</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
<!--====== banner end ==========-->

<!-- Market Section -->

<section id="team" class="team section-bg">
    <div class="container px-0">
        <div class="section-title">
            <div class="col-md-12 market-col">
                <h1 class="text-center"><span class="text_dark">Markets </span> <span class="text_light">We
                        Served</span></h1>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12  col-lg-6  pb-3 market-div">
                <div class="member d-flex align-items-start">
                    <div class="pic"><img class="img-fluid" src="<?php echo base_url(); ?>assets/images/market/1.jpg" alt=""></div>
                    <div class="member-info">
                        <h5 class="red">Bakery and Cereal Products</h5>
                        <p class="grey">We ensure you deliver products of a consistently superior standard
                            in your
                            high-throughput bakery with our tailor-made bakery solutions.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="" class="t">Encapsulated Ingredients</a></p>
                                <p> <a href="" class="t">Spray Dried Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="" class="t">Custom Nutrient Blends</a></p>
                                <p><a href="" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class=" col-md-12 col-lg-6 market-div pb-3">
                <div class="member d-flex align-items-start">
                    <div class="pic"><img class=" img-fluid" src="<?php echo base_url(); ?>assets/images/market/2.jpg" alt=""></div>
                    <div class="member-info">
                        <h5 class="red">Savory and Snack Products</h5>
                        <p class="grey">We ensure you deliver products of a consistently superior standard
                            in your
                            high-throughput bakery with our tailor-made bakery solutions.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="" class="t">Encapsulated Ingredients</a></p>
                                <p> <a href="" class="t">Spray Dried Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="" class="t">Custom Nutrient Blends</a></p>
                                <p><a href="" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="   col-md-12 col-lg-6 market-div pb-3">
                <div class="member d-flex align-items-start">
                    <div class="pic"><img class="img-fluid" src="<?php echo base_url(); ?>assets/images/market/3.jpg" alt=""></div>
                    <div class="member-info">
                        <h5 class="red">Savory and Snack Products</h5>
                        <p class="grey">We ensure you deliver products of a consistently superior standard
                            in your
                            high-throughput bakery with our tailor-made bakery solutions.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="" class="t">Encapsulated Ingredients</a></p>
                                <p> <a href="" class="t">Spray Dried Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="" class="t">Custom Nutrient Blends</a></p>
                                <p><a href="" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="  col-md-12 col-lg-6 market-div pb-3">
                <div class="member d-flex align-items-start">
                    <div class="pic"><img class="img-fluid" src="<?php echo base_url(); ?>assets/images/market/4.jpg" alt=""></div>
                    <div class="member-info">
                        <h5 class="red">Savory and Snack Products</h5>
                        <p class="grey">We ensure you deliver products of a consistently superior standard
                            in your
                            high-throughput bakery with our tailor-made bakery solutions.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="" class="t">Encapsulated Ingredients</a></p>
                                <p> <a href="" class="t">Spray Dried Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="" class="t">Custom Nutrient Blends</a></p>
                                <p><a href="" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="  col-md-12 col-lg-6 market-div pb-3">
                <div class="member d-flex align-items-start">
                    <div class="pic"><img class="img-fluid" src="<?php echo base_url(); ?>assets/images/market/5.jpg" alt=""></div>
                    <div class="member-info">
                        <h5 class="red">Savory and Snack Products</h5>
                        <p class="grey">We ensure you deliver products of a consistently superior standard
                            in your
                            high-throughput bakery with our tailor-made bakery solutions.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="" class="t">Encapsulated Ingredients</a></p>
                                <p> <a href="" class="t">Spray Dried Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="" class="t">Custom Nutrient Blends</a></p>
                                <p><a href="" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="  col-md-12 col-lg-6 market-divpb-3">
                <div class="member d-flex align-items-start">
                    <div class="pic"><img class="img-fluid" src="<?php echo base_url(); ?>assets/images/market/6.jpg" alt=""></div>
                    <div class="member-info">
                        <h5 class="red">Savory and Snack Products</h5>
                        <p class="grey">We ensure you deliver products of a consistently superior standard
                            in your
                            high-throughput bakery with our tailor-made bakery solutions.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="" class="t">Encapsulated Ingredients</a></p>
                                <p> <a href="" class="t">Spray Dried Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="" class="t">Custom Nutrient Blends</a></p>
                                <p><a href="" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-6 market-div pb-3">
                <div class="member d-flex align-items-start">
                    <div class="pic"><img class="img-fluid" src="<?php echo base_url(); ?>assets/images/market/7.jpg" alt=""></div>
                    <div class="member-info">
                        <h5 class="red">Savory and Snack Products</h5>
                        <p class="grey">We ensure you deliver products of a consistently superior standard
                            in your
                            high-throughput bakery with our tailor-made bakery solutions.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="" class="t">Encapsulated Ingredients</a></p>
                                <p> <a href="" class="t">Spray Dried Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="" class="t">Custom Nutrient Blends</a></p>
                                <p><a href="" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-6 market-div pb-3">
                <div class="member d-flex align-items-start">
                    <div class="pic"><img class="img-fluid" src="<?php echo base_url(); ?>assets/images/market/8.jpg" alt=""></div>
                    <div class="member-info">
                        <h5 class="red">Savory and Snack Products</h5>
                        <p class="grey">We ensure you deliver products of a consistently superior standard
                            in your
                            high-throughput bakery with our tailor-made bakery solutions.</p>
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <p> <a href="" class="t">Encapsulated Ingredients</a></p>
                                <p> <a href="" class="t">Spray Dried Ingredients</a></p>
                            </div>
                            <div class="col-md-6">
                                <p> <a href="" class="t">Custom Nutrient Blends</a></p>
                                <p><a href="" class="t">Granulated Products</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- End Of Market Section -->


<!-- Product Section Started -->

<section class="product pt-5 pb-5">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6 pro">
                <h2 class="text_dark">Our Products</h2>
                <p class="pro1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt
                    ut
                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco
                    laboris nisi ut aliquip ex ea commodo consequat.</p>

                <p class="text_orange">Explore All Products <span>&#8594;</span></p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo base_url(); ?>assets/images/product-1.jpg" alt="" class="img-fluid">
                <div class="text-block">
                    <p class="orange">Encapsulated Minerals</p>
                </div>
            </div>
        </div>
        <div class="padding">
            <div class="row no-gutters">
                <div class="col-md-3 mb-5">
                    <img src="<?php echo base_url(); ?>assets/images/product-2.jpg" alt="" class="img-fluid">
                    <div class="text-block1">
                        <p class="orange">Encapsulated Ingredients</p>
                    </div>
                </div>
                <div class="col-md-3 mb-5">
                    <img src="<?php echo base_url(); ?>assets/images/product-3.jpg" alt="" class="img-fluid">
                    <div class="text-block2">
                        <p class="orange">Encapsulated Bakery Ingredients</p>
                    </div>
                </div>
                <div class="col-md-3 mb-5">
                    <img src="<?php echo base_url(); ?>assets/images/product-4.jpg" alt="" class="img-fluid">
                    <div class="text-block3">
                        <p class="orange">Encapsulated Nutrients</p>
                    </div>
                </div>
                <div class="col-md-3 mb-5">
                    <img src="<?php echo base_url(); ?>assets/images/product-5.jpg" alt="" class="img-fluid">
                    <div class="text-block4">
                        <p class="orange">Encapsulated Food Additives</p>
                    </div>
                </div>
            </div>
        </div>

</section>


<!-- End of Product Section -->

<!-- Why sudeep nutrition -->

<section class="why pt-5 pb-5 extra-mr">
    <div class="container px-0">
        <div class="row">
            <div class="col-md-6">
                <div class="padding1">
                    <img src="<?php echo base_url(); ?>assets/images/2021-05-30.png" alt="" class="img-fluid">
                    <h3 class="text-white pt-5">Why Sudeep nutrition?</h3>
                    <p class="text1">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor
                        incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco
                        laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p class="text-white text-font">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                        eiusmod
                        tempor incididunt ut
                        labore et dolore magna aliqua.</p>

                    <a href="#home" class="btn_sec">Request a Sample <span>→</span></a>

                </div>
            </div>

            <div class="col-md-3">
                <div class="why-bx1">
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why_sudeep_1.svg" alt="" class="img-fluid">
                        <p class="t1">Best Quality</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why_sudeep_2.svg" alt="" class="img-fluid">
                        <p class="t1">Expert Scientists</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why_sudeep_3.svg" alt="" class="img-fluid">
                        <p class="t1">Advanced Technology</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                </div>

            </div>
            <div class="col-md-3">
                <div class="why-bx2">
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why_sudeep_4.svg" alt="" class="img-fluid">
                        <p class="t1">Best Price</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why_sudeep_5.svg" alt="" class="img-fluid">
                        <p class="t1">Premium raw material</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                    <div class="box3">
                        <img src="<?php echo base_url(); ?>assets/images/why_sudeep_6.svg" alt="" class="img-fluid">
                        <p class="t1">High-Tech Labs</p>
                        <p class="grey">Lorem ipsum dolor sit amet, consectetur ert adipiscing elit, sed do
                            eiusmod tempor
                            erto.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<!-- End Of Why sudeep -->


<!-- Latest Update Start -->

<section class="update">
    <div class="conatiner px-0">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h2 class="tag text-center pt-3 pb-0">Latest Updates and News</h2>
            </div>
        </div>


        <main>
            <div class="container bg-trasparent my-4 p-5 px-0" style="position: relative;">
                <div class="row row-cols-1 row-cols-xs-2 row-cols-sm-2 row-cols-lg-4 g-3">
                    <div class="col mt-4">
                        <div class="card1  card-news h-100 shadow-sm"> <img src="<?php echo base_url(); ?>assets/images/news 1.png" class="card-img-top"
                                alt="...">
                            <div class="card-body">
                                <p class="p1">Category Name | Date</p>
                                <p class="p2">Lorem ipsum dolor sit amet, consectetur.</p>
                                <p class="p3">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    officia
                                    in deserunt mollit anim id est sint laborum.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col mt-4">
                        <div class="card1 card-news h-100 shadow-sm"> <img src="<?php echo base_url(); ?>assets/images/news 2.png" class="card-img-top"
                                alt="...">
                            <div class="card-body">
                                <p class="p1">Instagram | Date</p>
                                <p class="p2">Lorem ipsum dolor sit amet, consectetur.</p>
                                <p class="p3">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    officia
                                    in deserunt mollit anim id est sint laborum.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col mt-4">
                        <div class="card1 card-news h-100 shadow-sm"> <img src="<?php echo base_url(); ?>assets/images/news 3.png" class="card-img-top"
                                alt="...">
                            <div class="card-body">
                                <p class="p1">Linked In | Date</p>
                                <p class="p2">Lorem ipsum dolor sit amet, consectetur.</p>
                                <p class="p3">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    officia
                                    in deserunt mollit anim id est sint laborum.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col mt-4">
                        <div class="card1 card-news h-100 shadow-sm"> <img src="<?php echo base_url(); ?>assets/images/news 4.png" class="card-img-top"
                                alt="...">
                            <div class="card-body">
                                <p class="p1">Youtube | Date</p>
                                <p class="p2">Lorem ipsum dolor sit amet, consectetur.</p>
                                <p class="p3">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    officia
                                    in deserunt mollit anim id est sint laborum.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </main>
    </div>
</section>


<!-- End of latest Update -->