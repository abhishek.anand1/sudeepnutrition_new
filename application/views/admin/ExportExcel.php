<?php
ob_start();
 session_start();


include("../dbconfig.php");
if ($_SESSION['logged-in'] !== true || $_SESSION['logged-role'] !== 'admin') {
// not logged in, move to login page
 header('Location: adminlogin.php');
 exit;
}
?>
<?php
//connect the database
//Headings of the excel columns
$contents="No,Name,EmailId,Mobile Number,Profession, Course,UTM source,UTM medium,UTM campaign,Date\n";
//Mysql query to get records from datanbase
$user_query = mysqli_query($conn,'SELECT * FROM enquiry');
//While loop to fetch the records
$i=1;

while($row = mysqli_fetch_array($user_query))
{
    foreach ($row as &$value) 
    {
            if (strpos($value, ',') !== false)
            {
                $value = htmlspecialchars_decode("\"".$value."\"");
            }

            $value = trim(preg_replace('/\s\s+/', ' ', $value));
    }
    $contents.=$i.",";
    $contents.=$row['name'].",";
    $contents.=$row['email'].",";
    $contents.=$row['mobile'].",";
    $contents.=$row['profession'].",";
    $contents.=$row['course'].",";
    $contents.=$row['utm_source'].",";
    $contents.=$row['utm_medium'].",";
    $contents.=$row['utm_campaign'].",";
    $contents.=$row['create_date']."\n";
    $i++;
}
//remove html and php tags etc.
$contents = strip_tags($contents);
//header to make force download the file
header("Content-Disposition: attachment; filename=ExcelReport".date("y-m-d").".csv");
print $contents;
//For more examples related PHP visit .com and free download..
?>