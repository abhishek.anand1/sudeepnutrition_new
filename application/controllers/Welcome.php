<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
     public function __construct() { 
         parent::__construct(); 
         // $this->load->helper(array('form', 'url')); 
         $this->load->model('Frontmodel');
         $this->load->model('M_Welcome');
      }

    public function index() {
        $data['title']="SCDL Home page";
        $data['keyword']="";
        $data['description']="SCDL Home page";
      
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('home' ,$data);
        $this->load->view('include/footer');
    }
    public function submitenquiry()
    {
    //  echo 'HI ENQUIRY I AM HRE TO SUBMIT';
    // die;
        date_default_timezone_set('asia/kolkata');
        $date=date('Y-m-d H:i:s');
        $data=array(
            
            'app_fname'=>htmlspecialchars($this->input->post('app_fname')),
            'app_email_address'=>htmlspecialchars($this->input->post('app_email_address')),
            'app_phone'=>htmlspecialchars($this->input->post('app_phone')),
            'app_city'=>htmlspecialchars($this->input->post('app_city')),
            'txtspeciality'=>htmlspecialchars($this->input->post('txtspeciality')),
            'app_course'=>htmlspecialchars($this->input->post('app_course')),
            'utm_source'=>htmlspecialchars($this->input->post('utm_source')),
            'utm_medium'=>htmlspecialchars($this->input->post('utm_medium')),
            'utm_campaign'=>htmlspecialchars($this->input->post('utm_campaign')),
            'date'=>$date
        );

        $query=$this->M_Welcome->insertenquiry($data);

            if($query=='1')
            {
                header("Location: thankyou");
            }
            else
            {
                header("Location: thankyou");
            }
    }
    

    public function technology_encapsulation() {
        
        // banner data
        // $program_id = $_GET['program_id'];
        $program_id = $this->input->get('program_id');
        
        $data['title']='SCDL Program';
        $data['keyword']="";
        $data['description']="SCDL Program";
        // echo $program_id;
        // die();
        // program highlights data
        // $data['banner']=$this->M_Welcome->getbannerdata($program_id);
        // // $data1['banner']=$this->M_Welcome->getbannerdata($program_id);
        // $data['program_highlights']=$this->M_Welcome->getprogramhighlightsdata($program_id);
        // $data['program_structure']=$this->M_Welcome->getprogramstructuredata($program_id);
        // $data['specialization_subject']=$this->M_Welcome->getprogramspecializationdata($program_id);
        // $data['career_option'] = $this->M_Welcome->get_career_option($program_id);
        // $data['eligibility'] = $this->M_Welcome->get_eligibility($program_id);
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('technology_encapsulation' ,$data);
        $this->load->view('include/footer');
    }  
    public function thankyou() {
        $data['title']="Thank you";
        $data['keyword']="";
        $data['description']="SCDL Home page";
      
        $this->load->view('include/commonscript',$data);
        $this->load->view('include/header');
        $this->load->view('thankyou' ,$data);
        $this->load->view('include/footer');
    }   
}

?>