$(document).ready(function() {

    $('[data-fancybox="images"]').fancybox({

    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 200) {
            $(".navbar").addClass("fixed-top");
        } else {
            $(".navbar").removeClass("fixed-top");
        }
    });

    $('.navbar-nav li a').on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        href = '#' + href.split('#').pop();

        var $target = $(href).offset().top - 0;

        $('html, body').animate({
            'scrollTop': $target
        }, 900, 'swing', function() {
            window.history.pushState("object or string", "Title", href);
        });
        if (screen.width <= 991) {
            $(".navbar-toggler").trigger('click');
        }
    });
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.scroll').fadeIn();
        } else {
            $('.scroll').fadeOut();
        }
    });
    $('#scroll').click(function() {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    // click scroll top
    $('#scroll1').click(function() {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    // COUNETR HOME PAGE
    var a = 0;
    jQuery(window).scroll(function() {

        var oTop = jQuery('#counter').offset().top - window.innerHeight;
        if (a == 0 && jQuery(window).scrollTop() > oTop) {
            jQuery('.counter_value').each(function() {
                var $this = jQuery(this),
                    countTo = $this.attr('data-count');
                jQuery({
                    countNum: $this.text()
                }).animate({
                        countNum: countTo
                    },

                    {

                        duration: 2000,
                        easing: 'swing',
                        step: function() {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function() {
                            $this.text(this.countNum);
                            //alert('finished');
                        }

                    });
            });
            a = 1;
        }

    });

    $("#success_stories").owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 8000,
        autoplayHoverPause: true,
        margin: 0,
        nav: false,
        dots: true,
        // navText: ["<img src='assets/css/carousel/left.png'>", "<img src='assets/css/carousel/right.png'>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });


    // Add down arrow icon for collapse element which is open by default
    $(".collapse.show").each(function() {
        $(this).prev(".card-header").find(".fa").addClass("fa-angle-down").removeClass("fa-angle-right");
    });

    // Toggle right and down arrow icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function() {
        $(this).prev(".card-header").find(".fa").removeClass("fa-angle-right").addClass("fa-angle-down");
    }).on('hide.bs.collapse', function() {
        $(this).prev(".card-header").find(".fa").removeClass("fa-angle-down").addClass("fa-angle-right");
    });



});




$('#gallery_carousel').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    navigation: false,
    margin: 10,
    ltr: true,
    loop: true,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 2
        },
        1000: {
            items: 3
        }
    }
});

$('#testimonial_carousel').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    navigation: false,
    margin: 10,
    ltr: true,
    loop: true,
    navText: ["<img src='assets/css/carousel/angle-left1.png'>", "<img src='assets/css/carousel/angle-right1.png'>"],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});

$('.modal').on('hidden.bs.modal', function() {
    var $this = $(this).find('iframe'),
        tempSrc = $this.attr('src');
    $this.attr('src', "");
    $this.attr('src', tempSrc);
});

// form

$('.form_toggle').css("right", "-330px");
$('.menu_icon').on('click', function() {
    if ($('.menu_icon').hasClass('open')) {
        $(this).removeClass('open');
        $(this).animate({
            "right": "-30px",
            "background-position": "0px"
        });
        $('.form_toggle').animate({ "right": "-330px" });
    } else {
        $(this).addClass('open');
        $(this).animate({
            "right": "290px",
            "background-position": "-40px"
        });
        $('.form_toggle').animate({ "right": "0px" });

    }
});